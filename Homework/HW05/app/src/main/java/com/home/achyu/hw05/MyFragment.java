package com.home.achyu.hw05;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Jesus on 3/18/2016.
 */
public class MyFragment extends Fragment {
    //Store
    private String title;
    private int page;

    public static MyFragment newInstance(int page, String title){
        MyFragment fragment = new MyFragment();
        Bundle args = new Bundle();
        args.putInt("1", page);
        args.putString("Page 1", title);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        page = getArguments().getInt("1", 0);
        title = getArguments().getString("Page 1");
    }

    //inflate the view for the fragment based on xml
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
//        TextView tvLabel = (TextView) view.findViewById(R.id.tvLabel);
//        tvLabel.setText(page + " -- " + title);
        return view;
    }
}
