package edu.uncc.mad.group14_hw06;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bradlamotte on 3/16/16.
 */
class ValidateAddressAsyncTask extends AsyncTask<City,Void,City> {
    ValidateAddressAsyncTask.IValidate activity;

    public ValidateAddressAsyncTask(IValidate activity){
        this.activity = activity;
    }

    @Override
    protected City doInBackground(City... params) {
        BufferedReader reader;
        StringBuilder builder = new StringBuilder();
        City city = params[0];
        String endpoint = "http://maps.googleapis.com/maps/api/geocode/json?address=" + city.toURL() + "&sensor=true&components=country:US";

        try {
            URL url = new URL(endpoint);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();
            if(statusCode==HttpURLConnection.HTTP_OK){
                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                builder.setLength(0);
                String line = reader.readLine();
                while (line != null) {
                    builder.append(line);
                    line = reader.readLine();
                }

                city.setValid(WeatherUtil.WeatherValidateParser.parseAddress(builder.toString(), city.toString()));
                return city;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(City city) {
        super.onPostExecute(city);
        this.activity.onAddressValidation(city);
    }

    public interface IValidate{
        public void onAddressValidation(City city);
    }
}
