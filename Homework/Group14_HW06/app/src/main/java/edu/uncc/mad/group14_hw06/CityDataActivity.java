package edu.uncc.mad.group14_hw06;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TabHost;

public class CityDataActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_data);

        City city = (City) getIntent().getExtras().getSerializable(MainActivity.CITY_KEY);
        Log.d("d", "city found on City Data " + city.toString());

        TabHost tabHost = getTabHost();

        // Tab for Hourly
        TabHost.TabSpec hourlySpec = tabHost.newTabSpec("Hourly");
        hourlySpec.setIndicator("Hourly");
        Intent hourlyIntent = new Intent(this, CityDataHourly.class);
        hourlyIntent.putExtra(MainActivity.CITY_KEY, city);
        hourlySpec.setContent(hourlyIntent);

        // Tab for Forecast
        TabHost.TabSpec forecastSpec = tabHost.newTabSpec("Forecast");
        forecastSpec.setIndicator("Forecast");
        Intent forecastIntent = new Intent(this, ForecastActivity.class);
        forecastIntent.putExtra(MainActivity.CITY_KEY, city);
        forecastSpec.setContent(forecastIntent);

        // Adding all TabSpec to TabHost
        tabHost.addTab(hourlySpec);
        tabHost.addTab(forecastSpec);

    }
}