package edu.uncc.mad.group14_hw06;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class CitiesTable {
    static final String TABLE_NAME = "cities";
    static final String COLUMN_KEY = "key";
    static final String COLUMN_NAME = "name";
    static final String COLUMN_STATE = "state";

    static public void onCreate(SQLiteDatabase db){
        StringBuilder sb = new StringBuilder();
        sb.append("create table " + TABLE_NAME + " (");
        sb.append(COLUMN_KEY + " text primary key not null, ");
        sb.append(COLUMN_NAME + " text not null, ");
        sb.append(COLUMN_STATE + " text not null);");

        try{
            db.execSQL(sb.toString());
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("drop table if exists " + TABLE_NAME);
        CitiesTable.onCreate(db);
    }
}

