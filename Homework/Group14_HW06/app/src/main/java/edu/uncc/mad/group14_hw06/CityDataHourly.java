package edu.uncc.mad.group14_hw06;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CityDataHourly extends Activity implements CityHourlyAsyncTask.IWeather{

    City city;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_data_hourly);

        city = (City) getIntent().getExtras().getSerializable(MainActivity.CITY_KEY);
        Log.d("d", "city found on City Data " + city.toString());

        TextView location = (TextView) findViewById(R.id.h_current_location);
        location.setText(city.toString());


        new CityHourlyAsyncTask(this).execute(city);
    }

    @Override
    public void onHourlyReceived(City city) {
        Log.d("d", "city hourly received " + city.getWeather_hourly().toString());
        final ListView list = (ListView) findViewById(R.id.hourlyList);
        CityHourlyDataAdapter adapter = new CityHourlyDataAdapter(this, R.layout.hourly_data_row, city.getWeather_hourly());
        list.setAdapter(adapter);
        adapter.setNotifyOnChange(true);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //go to Details activity
                Intent intent = new Intent(CityDataHourly.this, MainActivity.class);
               //pass extra weather objects
                startActivity(intent);
            }
        });

    }
}
