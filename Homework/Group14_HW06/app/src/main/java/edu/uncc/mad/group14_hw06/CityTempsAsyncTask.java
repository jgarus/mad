package edu.uncc.mad.group14_hw06;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class CityTempsAsyncTask extends AsyncTask<ArrayList<City>, Void, ArrayList<City>> {
    IWeather activity;
    String endpoint = "http://api.wunderground.com/api/56ad58108f3c4410/hourly/q/{state}/{city}.xml";

    public CityTempsAsyncTask(IWeather activity) {
        this.activity = activity;
    }

    @Override
    protected ArrayList<City> doInBackground(ArrayList<City>... params) {
        ArrayList<City> cities = params[0];

        for(City city : cities){
            getCityWeather(city);
        }

        return cities;
    }

    private void getCityWeather(City city){
        String cityEndpoint = endpoint.replace("{city}", city.getEncodedCity());
        cityEndpoint = cityEndpoint.replace("{state}", city.getState());

        try {
            URL url = new URL(cityEndpoint);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();
            if(statusCode == HttpURLConnection.HTTP_OK){
                InputStream in = con.getInputStream();
                city.setWeather_hourly(WeatherUtil.WeatherPullParser.parseWeatherDetails(in));
                //return city;
            }
            else {
                Log.d("demo", "Not found  " + statusCode);
                Log.d("demo", url.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(ArrayList<City> cities) {
        super.onPostExecute(cities);
        this.activity.onTempsReceived(cities);
    }

    interface IWeather{
        void onTempsReceived(ArrayList<City> cities);
    }
}
