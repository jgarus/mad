package edu.uncc.mad.group14_hw06;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class ForecastActivity extends Activity implements CityForecastAsyncTask.IWeather{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        City city = (City) getIntent().getExtras().getSerializable(MainActivity.CITY_KEY);

        TextView location = (TextView) findViewById(R.id.f_current_location);
        location.setText(city.toString());

        new CityForecastAsyncTask(this).execute(city);

    }


    @Override
    public void onForecastReceived(City city) {
        Log.d("d", "city forecasts received " + city.getForecasts().toString());
        ListView list = (ListView) findViewById(R.id.forecastList);
        ForecastAdapter adapter = new ForecastAdapter(this, R.layout.forecast_row, city.getForecasts());
        list.setAdapter(adapter);
        adapter.setNotifyOnChange(true);

                list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("d", "long clicked");
                //Go to Notes activity
                Intent intent = new Intent(ForecastActivity.this, MainActivity.class);
                //put extra, date and city
                startActivity(intent);
                return true;
            }
        });
    }
}
