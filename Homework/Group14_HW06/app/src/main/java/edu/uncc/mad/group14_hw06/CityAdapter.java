package edu.uncc.mad.group14_hw06;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class CityAdapter extends ArrayAdapter {
    List<City> mData;
    Context mcontext;
    int mResource;

    public CityAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.mData = objects;
        this.mcontext = context;
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource,parent,false);
        }

        City city = mData.get(position);
        TextView city_state = (TextView) convertView.findViewById(R.id.city_state);
        city_state.setText(city.toString());
        TextView temp = (TextView) convertView.findViewById(R.id.temp);

        ArrayList<Weather> weathers = city.getWeather_hourly();

        if(weathers.size() > 0){
            temp.setText(weathers.get(0).getTemperature() + "°F");
        }

        return convertView;
    }
}
