package edu.uncc.mad.group14_hw06;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements CityTempsAsyncTask.IWeather {
    static final int RESULT_CODE = 100;
    static final String key = "56ad58108f3c4410";
    static final String Key_One = "details";
    static final String CITY_KEY = "city";
    static final String RELOAD_KEY = "reload";
    ArrayList<City> cities = new ArrayList<>();
    TextView defaultMsg;
    ListView list;
    CityAdapter adapter;
    DataManager dm;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.d("d", "on create called");

        dm = new DataManager(MainActivity.this);
        defaultMsg = (TextView) findViewById(R.id.defaultMsg);
        list = (ListView) findViewById(R.id.citiesList);
        adapter = new CityAdapter(this, R.layout.city_row, cities);
        list.setAdapter(adapter);
        adapter.setNotifyOnChange(true);
        list.setBackgroundColor(Color.parseColor("#FFF5F2F2"));

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CityDataActivity.class);
                intent.putExtra(CITY_KEY, cities.get(position));
                startActivity(intent);
            }
        });

        list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("d", "long clicked position " + position);
                City city = cities.get(position);
                Log.d("d", "city " + city.toString());

                if (city != null) {
                    dm.deleteCity(city);
                    Toast.makeText(MainActivity.this, "City deleted", Toast.LENGTH_SHORT).show();
                    displayCities();
                }

                return true;
            }
        });

        displayCities();
    }

    private void displayCities(){
        cities = dm.getAllCities();
        Log.d("d", "displaying cities " + cities.toString());

        if(cities.size() > 0){
            defaultMsg.setVisibility(View.GONE);
        } else {
            defaultMsg.setVisibility(View.VISIBLE);
        }

        showProgress();
        new CityTempsAsyncTask(MainActivity.this).execute(cities);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

         if (requestCode == RESULT_CODE && resultCode == RESULT_OK){
            displayCities();
        }
    }

    @Override
    public void onTempsReceived(ArrayList<City> cities) {
        adapter.clear();
        adapter.addAll(cities);
        progress.dismiss();
    }

    public void showProgress(){
        progress = new ProgressDialog(MainActivity.this);
        progress.setMessage("Updating weather");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.menu_add_city:
                intent = new Intent(this, AddCityActivity.class);
                startActivityForResult(intent, MainActivity.RESULT_CODE);
                //startActivity(intent);
                return true;

            case R.id.menu_clear_cities:
                dm.deleteAllCities();
                Toast.makeText(MainActivity.this, "All cities cleared", Toast.LENGTH_SHORT).show();
                displayCities();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
