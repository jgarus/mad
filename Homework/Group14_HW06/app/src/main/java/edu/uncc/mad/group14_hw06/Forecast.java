package edu.uncc.mad.group14_hw06;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bradlamotte on 3/18/16.
 */
public class Forecast {
    Date date;
    int high =0, low=0, windSpeed, humidity;
    String condition, iconUrl, windDir, temperature;

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM");
        return dateFormat.format(this.date);
        //return date;
    }

    public void setDate(String dateStr) {
        SimpleDateFormat dateFormat;
        //7:00 PM MDT on March 18, 2016

        try {
            dateFormat = new SimpleDateFormat("h:mm a z 'on' MMMM d, yyyy");
            this.date = dateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }

    public int getLow() {
        return low;
    }

    public void setLow(int low) {
        this.low = low;
    }

    public int getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(int windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getWindDir() {
        return windDir;
    }

    public void setWindDir(String windDir) {
        this.windDir = windDir;
    }


    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }


    public String getLowTemp() {
        return String.valueOf(low);
    }

    public String getHighTemp() {
        return String.valueOf(high);
    }

    @Override
    public String toString() {
        return "Forecast{" +
                "date=" + date +
                ", high=" + high +
                ", low=" + low +
                ", windSpeed=" + windSpeed +
                ", humidity=" + humidity +
                ", condition='" + condition + '\'' +
                ", iconUrl='" + iconUrl + '\'' +
                ", windDir='" + windDir + '\'' +
                '}';
    }
}
