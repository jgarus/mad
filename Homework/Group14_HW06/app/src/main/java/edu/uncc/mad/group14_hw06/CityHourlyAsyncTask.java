package edu.uncc.mad.group14_hw06;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class CityHourlyAsyncTask extends AsyncTask<City, Void, City> {
    ProgressDialog progressDialog;
    IWeather activity;
    String endpoint = "http://api.wunderground.com/api/56ad58108f3c4410/hourly/q/{state}/{city}.xml";

    public CityHourlyAsyncTask(IWeather activity) {
        this.activity = activity;
    }

    @Override
    protected City doInBackground(City... params) {
        City city = params[0];

        String cityEndpoint = endpoint.replace("{city}", city.getEncodedCity());
        cityEndpoint = cityEndpoint.replace("{state}", city.getState());

        try {
            URL url = new URL(cityEndpoint);
            Log.d("d", "endpoint " + url.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();
            if(statusCode == HttpURLConnection.HTTP_OK){
                InputStream in = con.getInputStream();
                city.setWeather_hourly(WeatherUtil.WeatherPullParser.parseWeatherDetails(in));
                return city;
            }
            else {
                Log.d("demo", "Not found  " + statusCode);
                Log.d("demo", url.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog((Context) activity);
        progressDialog.setMessage("Loading Hourly Data");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }


    @Override
    protected void onPostExecute(City city) {
        super.onPostExecute(city);
        progressDialog.dismiss();
        this.activity.onHourlyReceived(city);
    }

    interface IWeather{
        void onHourlyReceived(City city);
    }
}