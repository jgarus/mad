package edu.uncc.mad.group14_hw06;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class CityDAO {
    private SQLiteDatabase db;

    public CityDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public long save(City city){
        ContentValues values = new ContentValues();
        values.put(CitiesTable.COLUMN_KEY, city.getKey());
        values.put(CitiesTable.COLUMN_NAME, city.getCity());
        values.put(CitiesTable.COLUMN_STATE, city.getState());
        return db.insert(CitiesTable.TABLE_NAME, null, values);
    }

    public boolean update(City city){
        ContentValues values = new ContentValues();
        values.put(CitiesTable.COLUMN_NAME, city.getCity());
        values.put(CitiesTable.COLUMN_STATE, city.getState());
        return db.update(CitiesTable.TABLE_NAME, values, CitiesTable.COLUMN_KEY + "=?", new String[]{city.getKey()}) > 0;
    }

    public boolean delete(City city){
        return db.delete(CitiesTable.TABLE_NAME, CitiesTable.COLUMN_KEY + "=?", new String[]{city.getKey()}) > 0;
    }

    public City get(String key){
        City city = null;

        Cursor c = db.query(true, CitiesTable.TABLE_NAME, new String[]{
                CitiesTable.COLUMN_KEY,
                CitiesTable.COLUMN_NAME,
                CitiesTable.COLUMN_STATE},
                CitiesTable.COLUMN_KEY + "=?", new String[]{key}, null, null, null, null, null);

        if(c != null && c.moveToFirst()){
            city = buildCityFromCursor(c);
            if(!c.isClosed()){
                c.close();
            }
        }

        return city;
    }

    public ArrayList<City> getAllCities(){
        ArrayList<City> cities = new ArrayList<>();

        Cursor c = db.query(true, CitiesTable.TABLE_NAME, new String[]{
                CitiesTable.COLUMN_KEY,
                CitiesTable.COLUMN_NAME,
                CitiesTable.COLUMN_STATE}, null, null, null, null, null, null, null);

        if(c != null && c.moveToFirst()){
            do{
                City city = buildCityFromCursor(c);
                if(city != null){
                    cities.add(city);
                }
            } while (c.moveToNext());

            if(!c.isClosed()){
                c.close();
            }
        }

        return cities;
    }

    private City buildCityFromCursor(Cursor c){
        City city = null;

        if(c != null){
            city = new City();
            city.setCity(c.getString(1));
            city.setState(c.getString(2));
        }

        return city;
    }

    public boolean deleteAll(){
        return db.delete(CitiesTable.TABLE_NAME, null, null) > 0;
    }

}


