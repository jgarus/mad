package edu.uncc.mad.group14_hw06;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jesus on 3/18/16.
 */
public class CityHourlyDataAdapter extends ArrayAdapter{

    List<Weather> mData;
    Context mContext;
    int mResource;

    public CityHourlyDataAdapter(Context context, int resource, List objects){
        super(context, resource, objects);
        this.mData = objects;
        this.mContext = context;
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }

        Weather weather = mData.get(position);

        TextView time = (TextView) convertView.findViewById(R.id.time);
        time.setText(weather.getTime());
        TextView condition = (TextView) convertView.findViewById(R.id.hourly_condition);
        condition.setText(weather.getClimateType());
        TextView temperature = (TextView) convertView.findViewById(R.id.temperature);
        temperature.setText(weather.getTemperature()+"°F");
        ImageView image = (ImageView) convertView.findViewById(R.id.hourly_icon);
        Picasso.with(mContext).load(weather.getIconUrl()).into(image);

        return convertView;
    }
}
