package edu.uncc.mad.group14_hw06;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class DataManager {
    private Context mContext;
    private DatabaseOpenHelper dbHelper;
    private SQLiteDatabase db;
    private CityDAO cityDAO;

    public DataManager(Context mContext) {
        this.mContext = mContext;
        this.dbHelper = new DatabaseOpenHelper(this.mContext);
        this.db = this.dbHelper.getWritableDatabase();
        this.cityDAO = new CityDAO(db);
    }

    public void close(){
        if(db != null){
            db.close();
        }
    }

    public CityDAO getCityDAO(){
        return this.cityDAO;
    }

    public long saveCity(City city){
        return this.cityDAO.save(city);
    }

    public boolean updateCity(City city){
        return this.cityDAO.update(city);
    }

    public boolean deleteCity(City city){
        return this.cityDAO.delete(city);
    }

    public City getCity(String key){
        return this.cityDAO.get(key);
    }

    public ArrayList<City> getAllCities(){
        return this.cityDAO.getAllCities();
    }

    public void upgrade(){
        CitiesTable.onUpgrade(this.db, 0, 1);
    }

    public void create(){
        CitiesTable.onCreate(this.db);
    }

    public boolean deleteAllCities(){
        return this.cityDAO.deleteAll();
    }
}

