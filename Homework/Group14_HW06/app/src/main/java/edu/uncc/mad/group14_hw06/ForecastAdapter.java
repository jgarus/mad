package edu.uncc.mad.group14_hw06;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


import com.squareup.picasso.Picasso;


/**
 * Created by jesus on 3/18/16.
 */
public class ForecastAdapter extends ArrayAdapter{
    List<Forecast> mData;
    Context mContext;
    int mResource;


    public ForecastAdapter(Context context, int resource, List objects) {
        super(context, resource, objects);
        this.mData = objects;
        this.mContext = context;
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource,parent,false);
        }

        Forecast forecast = mData.get(position);


        TextView date = (TextView) convertView.findViewById(R.id.forecast_date);
        date.setText(forecast.getDate().toString());
        TextView condition = (TextView) convertView.findViewById(R.id.forecast_condition);
        condition.setText(forecast.getCondition());
        ImageView image = (ImageView) convertView.findViewById(R.id.forecast_icon);
        Picasso.with(mContext).load(forecast.getIconUrl()).into(image);
        TextView temperature_high = (TextView) convertView.findViewById(R.id.temperature_high);
        temperature_high.setText(forecast.getHighTemp()+"°F");
        TextView temperature_low = (TextView) convertView.findViewById(R.id.temperature_low);
        temperature_low.setText("/"+forecast.getLowTemp()+"°F");

        return convertView;
    }
}
