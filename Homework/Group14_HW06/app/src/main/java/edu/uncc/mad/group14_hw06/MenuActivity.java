package edu.uncc.mad.group14_hw06;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

/**
 * Created by bradlamotte on 3/16/16.
 */
public class MenuActivity extends AppCompatActivity {
    protected DataManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("d", "onCreate Menu called");
        super.onCreate(savedInstanceState);
        dm = new DataManager(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);

//        if(!(this instanceof MainActivity)){
//            menu.findItem(R.id.menu_add_city).setVisible(false);
//            menu.findItem(R.id.menu_notes).setVisible(false);
//        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.menu_add_city:
                intent = new Intent(this, AddCityActivity.class);
                startActivityForResult(intent, MainActivity.RESULT_CODE);
                //startActivity(intent);
                return true;

            case R.id.menu_clear_cities:
                dm.deleteAllCities();
                Toast.makeText(this, "All cities cleared", Toast.LENGTH_SHORT).show();
                //displayCities();
                intent = getIntent();
                finish();
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
