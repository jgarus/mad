package edu.uncc.mad.group14_hw06;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AddCityActivity extends AppCompatActivity implements ValidateAddressAsyncTask.IValidate {
    EditText input;
    Intent intent;
    DataManager dm;
    ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);

        dm = new DataManager(AddCityActivity.this);

        findViewById(R.id.savecity_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            City city = new City();
            input = (EditText) findViewById(R.id.city_value);
            city.setCity(input.getText().toString());
            input = (EditText) findViewById(R.id.state_value);
            city.setState(input.getText().toString());

                if (city.getCity() != null && city.getState() != null) {
                showSearchingProgress();
                new ValidateAddressAsyncTask(AddCityActivity.this).execute(city);
            } else
                Toast.makeText(AddCityActivity.this, "All Fields are Required", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showSearchingProgress(){
        progress = new ProgressDialog(AddCityActivity.this);
        progress.setMessage("Saving City");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public void onAddressValidation(City city) {
        Log.d("d", "onAddressValidation " + city.toString());
        progress.dismiss();

        if (city.isValid()) {
            dm.saveCity(city);
            intent = new Intent(AddCityActivity.this, MainActivity.class);
            //intent.putExtra(MainActivity.RELOAD_KEY, true);
            //Log.d("d", "sending back to main with " + intent.getExtras().toString());
            setResult(RESULT_OK, intent);
            //startActivity(intent);
            finish();
        } else {
            Toast.makeText(this, "Enter a Valid Location", Toast.LENGTH_LONG).show();
        }
    }
}
