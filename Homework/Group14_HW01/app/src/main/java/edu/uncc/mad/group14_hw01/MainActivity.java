// Homework 01
// Group 14
// Achyuta Ram Rampalli, Brad LaMotte, Jesus Garcia

package edu.uncc.mad.group14_hw01;

import android.app.ActionBar;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    TextView error_msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.calc_ic_launcher);
        Button calc_btn = (Button)findViewById(R.id.calculate_button);

        error_msg = (TextView)findViewById(R.id.error_display);

        final EditText input_id = (EditText) findViewById(R.id.budget);
        final CompoundButton delivery_switch = (CompoundButton) findViewById(R.id.delivery_switch);
        final TextView price = (TextView) findViewById(R.id.price_display);
        final SeekBar tip_seek = (SeekBar) findViewById(R.id.tip_seekbar);
        final TextView status = (TextView) findViewById(R.id.status_display);

        input_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                error_msg.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        calc_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (input_id.getText().toString().isEmpty()| input_id.getText().toString().equals(".")) {
                    setError();

                } else {

                    //Set and parse value of the user input
                    String usrPrice = input_id.getText().toString();
                    float usrPriceFinal = Float.parseFloat(usrPrice);

                    int memory = getValueFromRadioGroup(R.id.memory_radios);
                    int storage = getValueFromRadioGroup(R.id.storage_radios);

                    ArrayList<Integer> arr = new ArrayList<Integer>();
                    arr.add(R.id.accessory_mouse);
                    arr.add(R.id.accessory_flash);
                    arr.add(R.id.accessory_case);
                    arr.add(R.id.accessory_pad);

                    int accessories_count = getCheckItemsCount(arr);
                    final TextView tip_display = (TextView) findViewById(R.id.tip_display);
                    int tip = Integer.parseInt(tip_display.getText().toString().replace("%", ""));
                    int delivery = 0;

                    if (delivery_switch.isChecked()) {
                        delivery = 1;
                    }
                    float cost = (float) (((10 * memory + 0.75 * storage + 20 * accessories_count) * (1 + tip / 100.0)) + 5.95 * delivery);
                    DecimalFormat round = new DecimalFormat("0.00");
                    price.setText("$" + round.format(cost) + "");

                    //Comparing if user is within or over budget, colors also set accordingly
                    if (cost <= usrPriceFinal){
                        status.setText("Within Budget");
                        status.setBackgroundResource(R.color.colorBackgroundWithin);
                        status.setTextColor(Color.WHITE);
                    }
                    else {
                        status.setText("Over Budget");
                        status.setBackgroundResource(R.color.colorBacgroundOver);
                        status.setTextColor(Color.WHITE);
                    }

                }

            }
        });

        Button btw_reset = (Button) findViewById(R.id.reset_button);
        btw_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Reset display labels
                input_id.setText("");
                price.setText("");
                status.setText("");
                status.setBackgroundResource(R.color.colorStatusText);
               //Reset delivery switch to on
                delivery_switch.setChecked(true);
                //Reset tip bar to 3 (default * 5 = 15)
                tip_seek.setProgress(3);
                resetAddons(v);
            }
        });

        tip_seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Log.d("debug", "tip: " + progress);
                TextView display = (TextView) findViewById(R.id.tip_display);
                display.setText((progress * 5) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }

    public int getValueFromRadioGroup(int group_id){
        RadioGroup rg = (RadioGroup) findViewById(group_id);
        RadioButton id = (RadioButton)findViewById(rg.getCheckedRadioButtonId());
        String value = id.getText().toString();
        if(value.equals("1TB")){
            value = value.replace("1TB","1024");
        }
        else
            value = value.replace("GB","");
        return Integer.parseInt(value);
    }

    public int getCheckItemsCount(ArrayList<Integer> checkbox_list){
        int count=0;
        for(int i=0;i<checkbox_list.size();i++){
            CheckBox check = (CheckBox) findViewById(checkbox_list.get(i));
            if(check.isChecked()){
                count++;
            }
        }
        return count;
    }

    //Reset all checkboxes and radio buttons
    public void resetAddons(View v){
        CheckBox chkBox1 = (CheckBox) findViewById(R.id.accessory_mouse);
        CheckBox chkBox2 = (CheckBox) findViewById(R.id.accessory_flash);
        CheckBox chkBox3 = (CheckBox) findViewById(R.id.accessory_pad);
        CheckBox chkBox4 = (CheckBox) findViewById(R.id.accessory_case);

        RadioButton rdoStor1 = (RadioButton) findViewById(R.id.storage_250);
        RadioButton rdoMem1 = (RadioButton) findViewById(R.id.memory_2);

        chkBox1.setChecked(false);
        chkBox2.setChecked(false);
        chkBox3.setChecked(false);
        chkBox4.setChecked(false);

        rdoStor1.setChecked(true);
        rdoMem1.setChecked(true);
    }


    public void setError(){
        error_msg.setVisibility(View.VISIBLE);
    }
}
