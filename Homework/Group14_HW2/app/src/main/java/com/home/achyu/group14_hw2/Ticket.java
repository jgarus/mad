package com.home.achyu.group14_hw2;



        import android.util.Log;

        import java.io.Serializable;
        import java.text.DateFormat;
        import java.text.ParseException;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.Date;
        import java.util.Random;

/**
 * Created by achyu on 05-Feb-16.
 */
public class Ticket implements Serializable{
    int id;
    String name;
    String source;
    String destination;
    boolean round_trip;
    Date departure_date;
    Date returning_date;
    ArrayList<String> errors;


    public Ticket() {
        this.setId();
        this.name = "";
        this.source = "";
        this.destination = "";
        this.errors = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId() {
        Random generator = new Random();
        this.id = generator.nextInt(10000);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public boolean isRound_trip() {
        return round_trip;
    }

    public void setRound_trip(boolean round_trip) {
        this.round_trip = round_trip;
    }

    public Date getDeparture_date() {
        return departure_date;
    }

    public void setDeparture_date(String date_str) {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy h:mm a");
        try {
            this.departure_date = formatter.parse(date_str);
        } catch (ParseException e){
            this.departure_date = null;
        }
    }

    public Date getReturning_date() {
        return returning_date;
    }

    public void setReturning_date(String date_str) {
        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy h:mm a");
        try {
            this.returning_date = formatter.parse(date_str);
        } catch (ParseException e){
            this.returning_date = null;
        }
    }

    public String toString(){
        return "id:" + this.id + " name:" + this.name + " source:" + this.source +
                " destination:" + this.destination + " roud:" + this.round_trip +
                " depart:" + this.departure_date +
                " return:" + this.returning_date;
    }

    public Boolean valid(){
        this.errors.clear();
        return requiredFields() && duplicateDestination() && validDepartureDate() && validDateOrder();
    }

    private Boolean requiredFields(){
        if((this.name == null) || this.name.isEmpty() ||
                (this.source == null) || this.source.isEmpty() ||
                (this.destination == null) || this.destination.isEmpty() ||
                (this.departure_date == null) ||
                (this.isRound_trip() && this.returning_date == null)){
            this.errors.add("Please complete all fields");
            return false;
        } else
            return true;
    }

    private Boolean duplicateDestination(){
        if(this.source.equals(this.destination)){
            this.errors.add("Destination must be different than source");
            return false;
        } else {
            return true;
        }
    }

    private Boolean validDepartureDate(){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.HOUR, -3);

        Log.d("proj", "now " + now.toString());
        Log.d("proj", "depart " + this.departure_date.toString());
        if(this.departure_date != null && now.after(this.departure_date)){
            this.errors.add("Departure date must be in the future");
            return false;
        } else{
            return true;
        }
    }

    private Boolean validDateOrder(){
        if(this.returning_date != null && this.departure_date.after(this.returning_date)){
            this.errors.add("Return date must be after departure");
            return false;
        } else {
            return true;
        }
    }

    public String getErrorMessages(){
        StringBuilder builder = new StringBuilder();
        for(String s : this.errors) {
            builder.append(s);
        }
        return builder.toString();
    }

}