package com.home.achyu.group14_hw2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrintActivity extends AppCompatActivity {
    final static String CREATE_KEY = "create";
    TextView field;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);
        Ticket ticket = new Ticket();
        ticket = (Ticket) getIntent().getExtras().getSerializable(MainActivity.TICKET_KEY);


        field = (TextView) findViewById(R.id.name_value);
        field.setText(ticket.getName());
        field = (TextView) findViewById(R.id.source_value);
        field.setText(ticket.getSource());
        field = (TextView) findViewById(R.id.destination_value);
        field.setText(ticket.getDestination());
        field = (TextView) findViewById(R.id.departure_value);
        field.setText(toDateTime(ticket.getDeparture_date()));
        if(ticket.isRound_trip()){
            findViewById(R.id.return_label).setVisibility(View.VISIBLE);
            field = (TextView) findViewById(R.id.return_value);
            field.setText(toDateTime(ticket.getReturning_date()));
        }

        final Ticket finalTicket = ticket;
        findViewById(R.id.finish_button_print).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("proj", "ticket from print: " + finalTicket.toString());
                Intent intent = new Intent();
                intent.putExtra(MainActivity.TICKET_KEY,finalTicket);
                Log.d("demo","in print"+finalTicket.getId());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

    public String toDateTime(Date date){
        String datetime;
        DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy, hh:mm aa");
        datetime = dateformat.format(date);
        return datetime;
    }
}
