package com.home.achyu.group14_hw2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.util.ArrayList;

public class ViewActivity extends AppCompatActivity {
    int ticket_index = 0;
    ArrayList<Ticket> tickets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);

        tickets = (ArrayList<Ticket>) getIntent().getExtras().getSerializable(MainActivity.TICKET_KEY);

        final TicketForm form = new TicketForm(this);
        form.disableForm();
        showTicket(ticket_index);

        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticket_index++;
                showTicket(ticket_index);
            }
        });

        findViewById(R.id.prev_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticket_index--;
                showTicket(ticket_index);
            }
        });

        findViewById(R.id.first_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticket_index = 0;
                showTicket(ticket_index);
            }
        });

        findViewById(R.id.last_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticket_index = (tickets.size()-1);
                showTicket(ticket_index);
            }
        });

        findViewById(R.id.finish_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void showTicket(int index){
        if(!tickets.isEmpty()) {
            if ((index + 1) > tickets.size()) {
                ticket_index = 0;
            } else if (index < 0) {
                ticket_index = (tickets.size() - 1);
            }

            TicketForm form = new TicketForm(this);
            Ticket ticket = tickets.get(ticket_index);
            form.setName(ticket.getName());
            form.setSource(ticket.getSource());
            form.setDestination(ticket.getDestination());
            form.setRoundTrip(ticket.isRound_trip());
            form.setDepartureDate(ticket.getDeparture_date());
            if (ticket.isRound_trip()) {
                form.setReturnDate(ticket.getReturning_date());
            }
            form.showReturnDate();
        }
    }
}
