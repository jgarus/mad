package com.home.achyu.group14_hw2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import java.util.ArrayList;

public class DeleteActivity extends AppCompatActivity {
    static int ticket_id ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);

        final ArrayList<Ticket> tickets= (ArrayList<Ticket>) getIntent().getExtras().getSerializable(MainActivity.TICKET_KEY);

        final TicketForm form = new TicketForm(this);
        form.disableForm();

        String[] names = getNames(tickets);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
            .setTitle("Select a Title")
            .setItems(names, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Ticket ticket = tickets.get(which);
                    ticket_id = ticket.getId();
                    form.setName(ticket.getName());
                    form.setSource(ticket.getSource());
                    form.setDestination(ticket.getDestination());
                    form.setRoundTrip(ticket.isRound_trip());
                    form.setDepartureDate(ticket.getDeparture_date());
                    if (ticket.isRound_trip()) {
                        form.setReturnDate(ticket.getReturning_date());
                    }
                    form.showReturnDate();
                }
            });
        final AlertDialog names_dialog = builder.create();
        makeViewNonEditable((LinearLayout) findViewById(R.id.form));

        findViewById(R.id.select_ticket_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                names_dialog.show();
            }
        });

        findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ticket_id > 0) {
                    Intent intent = new Intent();
                    intent.putExtra(MainActivity.TICKET_KEY, ticket_id);
                    setResult(MainActivity.RESULT_OK, intent);
                    finish();
                }
            }
        });

        findViewById(R.id.cancel_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private String[] getNames(ArrayList<Ticket> tickets){
        String[] names = new String[tickets.size()];
        int index = 0;

        for(Ticket t : tickets){
            names[index] = t.getName();
            index++;
        }

        return names;
    }

    protected void makeViewNonEditable(LinearLayout layout){
        for(int i=0;i<layout.getChildCount();i++){
            View v =layout.getChildAt(i);
            if(v instanceof RadioButton){
                ((RadioButton) v).setKeyListener(null);
            }

        }
    }
}
