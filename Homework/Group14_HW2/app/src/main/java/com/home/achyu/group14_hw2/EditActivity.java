package com.home.achyu.group14_hw2;


        import android.app.AlertDialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.Button;
        import android.widget.Toast;

        import java.util.ArrayList;

public class EditActivity extends AppCompatActivity {
    public static String EDIT_KEY = "edit";
    static int ticket_id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);
        final ArrayList<Ticket> tickets = (ArrayList<Ticket>) getIntent().getExtras().getSerializable(MainActivity.TICKET_KEY);


        final TicketForm form  = new TicketForm(this);
        form.run();
        form.disableForm();
        String[] names = getNames(tickets);

        //Alert dialog for names of tickets to edit
        final AlertDialog.Builder builder = new AlertDialog.Builder(this)
            .setTitle("Pick a Ticket")
            .setItems(names, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                Log.d("proj", "name clicked " + which);
                Ticket ticket = tickets.get(which);
                ticket_id = ticket.getId();
                form.setName(ticket.getName());
                form.setSource(ticket.getSource());
                form.setDestination(ticket.getDestination());
                form.setRoundTrip(ticket.isRound_trip());
                form.setDepartureDate(ticket.getDeparture_date());
                if(ticket.isRound_trip()){
                    form.setReturnDate(ticket.getReturning_date());
                }
                form.enableForm();
                }
            });

        final AlertDialog namesDialog =  builder.create();
        Button select_button = (Button) findViewById(R.id.select_ticket_delete);
        select_button.setOnClickListener(new View.OnClickListener(){
             @Override
             public void onClick (View v){
                namesDialog.show();
             }
         });


        Button save_ticket = (Button) findViewById(R.id.save_ticket);
        save_ticket.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v){
                Ticket ticket = new Ticket();
                ticket.id = ticket_id;
                ticket.setName(form.getName());
                ticket.setSource(form.getSource());
                ticket.setDestination(form.getDestination());
                ticket.setRound_trip(form.getRoundTrip());
                ticket.setDeparture_date(form.getDepartureDate());
                if(form.getRoundTrip()){
                    ticket.setReturning_date(form.getReturnDate());
                }
                final Ticket final_ticket = ticket;
                if(ticket.valid()){
                    //send activity
                    Intent intent = new Intent(EditActivity.this, PrintActivity.class);
                    intent.putExtra(MainActivity.TICKET_KEY, final_ticket);
                    startActivityForResult(intent,MainActivity.REQ_CODE_EDIT);
                } else {
                    Toast.makeText(getApplicationContext(), ticket.getErrorMessages(), Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private String[] getNames(ArrayList<Ticket> tickets){
        String[] names = new String[tickets.size()];
        int index = 0;

        for(Ticket t : tickets){
            names[index] = t.getName();
            index++;
        }

        return names;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainActivity.REQ_CODE_EDIT){
            if(resultCode == RESULT_OK){
                setResult(RESULT_OK,data);
                finish();
            }
        }
    }
}

