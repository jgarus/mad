package com.home.achyu.group14_hw2;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by bradlamotte on 2/5/16.
 */
public class TicketForm {
    private AppCompatActivity activity;

    String[] locations = {
            "Albany, NY",
            "Atlanta, GA",
            "Boston, MA",
            "Charlotte, NC",
            "Chicago, IL",
            "Greenville, SC",
            "Houston, TX",
            "Las Vegas, NV",
            "Los Angeles, CA",
            "Miami, FL",
            "Myrtle Beach, SC",
            "New York, NY",
            "Portland, OR",
            "Raleigh, NC",
            "San Jose, CA",
            "Washington, DC"
    };

    TicketForm(AppCompatActivity activity){
        this.activity = activity;
    }

    public void run() {
        final AppCompatActivity this_activity = this.activity;


    }

    private String formatTime(int hour, int minute){
        int current_hour = hour;
        String amPm = "AM";

        if(hour > 11 && minute > 0){
            current_hour = hour - 12;
        }

        if(hour == 0){
            current_hour = 12;
        }

        if(hour > 11){
            amPm = "PM";
        }

        return current_hour + ":" + String.format("%02d", minute) + " " + amPm;
    }

    public String getName(){
        EditText nameField = (EditText) this.activity.findViewById(R.id.name);
        return nameField.getText().toString();
    }

    public void setName(String name){
        EditText nameField = (EditText) this.activity.findViewById(R.id.name);
        nameField.setText(name);
    }

    public String getSource(){
        EditText field = (EditText) this.activity.findViewById(R.id.source);
        field.setKeyListener(null);
        return field.getText().toString();
    }

    public void setSource(String name){
        EditText field = (EditText) this.activity.findViewById(R.id.source);
        field.setText(name);
    }

    public String getDestination(){
        EditText field = (EditText) this.activity.findViewById(R.id.destination);
        field.setKeyListener(null);
        return field.getText().toString();
    }

    public void setDestination(String name){
        EditText field = (EditText) this.activity.findViewById(R.id.destination);
        field.setText(name);
    }

    public Boolean getRoundTrip(){
        RadioGroup rg = (RadioGroup) this.activity.findViewById(R.id.trip_radios);
        return (rg.getCheckedRadioButtonId() != R.id.trip_one_way);
    }

    public void setRoundTrip(Boolean value){
        RadioGroup rg = (RadioGroup) this.activity.findViewById(R.id.trip_radios);
        if(value){
            RadioButton button = (RadioButton) this.activity.findViewById(R.id.radioButton);
            button.setChecked(true);
        } else {
            RadioButton button = (RadioButton) this.activity.findViewById(R.id.trip_one_way);
            button.setChecked(true);
        }
    }

    public String getDepartureDate(){
        EditText departureDate = (EditText) this.activity.findViewById(R.id.departure_date);
        EditText departureTime = (EditText) this.activity.findViewById(R.id.departure_time);
        return departureDate.getText().toString() + " " + departureTime.getText().toString();
    }

    public void setDepartureDate(Date d){
        EditText departureDate = (EditText) this.activity.findViewById(R.id.departure_date);
        departureDate.setKeyListener(null);
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
        departureDate.setText(formatter.format(d));
        formatter = new SimpleDateFormat("h:mm a");
        EditText departureTime = (EditText) this.activity.findViewById(R.id.departure_time);
        departureTime.setKeyListener(null);
        departureTime.setText(formatter.format(d));
    }

    public String getReturnDate(){
        EditText returnDate = (EditText) this.activity.findViewById(R.id.return_date);
        EditText returnTime = (EditText) this.activity.findViewById(R.id.return_time);
        return returnDate.getText().toString() + " " + returnTime.getText().toString();
    }

    public void setReturnDate(Date d){
        Log.d("proj", "return date: " + d.toString());
        if(d != null) {
            EditText departureDate = (EditText) this.activity.findViewById(R.id.return_date);
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            departureDate.setText(formatter.format(d));
            formatter = new SimpleDateFormat("h:mm a");
            EditText departureTime = (EditText) this.activity.findViewById(R.id.return_time);
            departureTime.setText(formatter.format(d));
        }
    }

    public void disableForm(){
        EditText name = (EditText) this.activity.findViewById(R.id.name);
        name.setEnabled(false);

        RadioButton button = (RadioButton) this.activity.findViewById(R.id.trip_one_way);
        button.setEnabled(false);
        button = (RadioButton) this.activity.findViewById(R.id.radioButton);
        button.setEnabled(false);
    }

    public void enableForm(){
        EditText name = (EditText) this.activity.findViewById(R.id.name);
        name.setEnabled(true);
        final AppCompatActivity this_activity = this.activity;

        // Build Sources alert dialog
        AlertDialog.Builder sourceBuilder = new AlertDialog.Builder(this.activity);
        sourceBuilder.setTitle("Source")
                .setItems(locations, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText source = (EditText) this_activity.findViewById(R.id.source);
                        source.setText(locations[which]);
                    }
                });
        final AlertDialog sourcesAlert = sourceBuilder.create();

        // Show dropdown when click Source field
        this.activity.findViewById(R.id.source).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sourcesAlert.show();
            }
        });

        // Build Destinations alert dialog
        AlertDialog.Builder destinationBuilder = new AlertDialog.Builder(this_activity);
        destinationBuilder.setTitle("Destination")
                .setItems(locations, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        EditText destination = (EditText) this_activity.findViewById(R.id.destination);
                        destination.setText(locations[which]);
                    }
                });
        final AlertDialog destinationAlert = destinationBuilder.create();

        // Show dropdown when click Destination field
        this_activity.findViewById(R.id.destination).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destinationAlert.show();
            }
        });

        Calendar calendar = Calendar.getInstance(Locale.US);

        // Create departure date popup
        final DatePickerDialog departDateDialog = new DatePickerDialog(this_activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                EditText departure_date = (EditText) this_activity.findViewById(R.id.departure_date);
                departure_date.setText(String.format("%02d/%02d/%4d",monthOfYear+1,dayOfMonth,year));
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        this_activity.findViewById(R.id.departure_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departDateDialog.show();
            }
        });

        // Create departure time popup
        final TimePickerDialog departTimeDialog = new TimePickerDialog(this_activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                EditText departure_time = (EditText) this_activity.findViewById(R.id.departure_time);
                departure_time.setText(formatTime(hourOfDay, minute));
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);

        this_activity.findViewById(R.id.departure_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                departTimeDialog.show();
            }
        });


        calendar.add(Calendar.DATE, 3);

        // Create return date popup
        final DatePickerDialog returnDateDialog = new DatePickerDialog(this_activity, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                EditText return_date = (EditText) this_activity.findViewById(R.id.return_date);
                return_date.setText((monthOfYear+1) + "/" + dayOfMonth + "/" + year);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        this_activity.findViewById(R.id.return_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnDateDialog.show();
            }
        });

        // Create return time popup
        final TimePickerDialog returnTimeDialog = new TimePickerDialog(this_activity, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                EditText return_time = (EditText) this_activity.findViewById(R.id.return_time);
                return_time.setText(formatTime(hourOfDay, minute));
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);

        this_activity.findViewById(R.id.return_time).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnTimeDialog.show();
            }
        });

        RadioButton button = (RadioButton) this.activity.findViewById(R.id.trip_one_way);
        button.setEnabled(true);
        button = (RadioButton) this.activity.findViewById(R.id.radioButton);
        button.setEnabled(true);

        // Hide return date/time depending on trip radio value
        RadioGroup rg = (RadioGroup) this_activity.findViewById(R.id.trip_radios);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                showReturnDate();
            }
        });
        showReturnDate();
    }

    public void showReturnDate(){
        RadioGroup rg = (RadioGroup) this.activity.findViewById(R.id.trip_radios);
        int checkedId = rg.getCheckedRadioButtonId();
        LinearLayout return_section = (LinearLayout) this.activity.findViewById(R.id.return_section);

        if (checkedId == R.id.trip_one_way) {
            EditText return_date = (EditText) this.activity.findViewById(R.id.return_date);
            return_date.setText("");
            EditText return_time = (EditText) this.activity.findViewById(R.id.return_time);
            return_time.setText("");
            return_section.setVisibility(View.INVISIBLE);
        } else {
            return_section.setVisibility(View.VISIBLE);
        }
    }
}
