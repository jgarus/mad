package com.home.achyu.group14_hw2;

        import android.app.AlertDialog;
        import android.app.DatePickerDialog;
        import android.app.TimePickerDialog;
        import android.content.DialogInterface;
        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.View;
        import android.widget.DatePicker;
        import android.widget.EditText;
        import android.widget.LinearLayout;
        import android.widget.RadioGroup;
        import android.widget.TimePicker;
        import android.widget.Toast;

        import java.text.SimpleDateFormat;
        import java.util.Calendar;
        import java.util.Date;

public class CreateActivity extends AppCompatActivity {

    final static Integer PRINT_CODE = 1;
    final static String PRINT_KEY ="print";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);
        final TicketForm form = new TicketForm(this);
        form.enableForm();

        findViewById(R.id.create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Ticket ticket = new Ticket();
            EditText name = (EditText) findViewById(R.id.name);
            ticket.setName(form.getName());
            ticket.setSource(form.getSource());
            ticket.setDestination(form.getDestination());
            ticket.setRound_trip(form.getRoundTrip());
            ticket.setDeparture_date(form.getDepartureDate());
            if(form.getRoundTrip()){
                ticket.setReturning_date(form.getReturnDate());
            }

            if(ticket.valid()){
                //send activity
                Log.d("proj", "sending");
                Intent intent = new Intent(CreateActivity.this, PrintActivity.class);
                intent.putExtra(MainActivity.TICKET_KEY, ticket);
                startActivityForResult(intent, MainActivity.REQ_CODE_CREATE);
            } else {
                Toast.makeText(getApplicationContext(), ticket.getErrorMessages(), Toast.LENGTH_SHORT).show();
            }

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainActivity.REQ_CODE_CREATE){
            if (resultCode == RESULT_OK){
                Intent intent = new Intent();
                setResult(RESULT_OK, data);
                finish();
            }
        }

    }

}
