package com.home.achyu.group14_hw2;


import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {
    Intent intent;
    public static final int REQ_CODE_CREATE = 50;
    public static final int REQ_CODE_EDIT = 100;
    public static final int REQ_CODE_DELETE = 150;
    final static String TICKET_KEY = "ticket";
    final LinkedList<Ticket> tickets = new LinkedList();
    Ticket ticket;
    static int index;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.drawable.ic_action_name);
        TextView service = (TextView) findViewById(R.id.care_number);
        service.setText(Html.fromHtml("<u>Customer Care: 999-999-9999</u>"));

        findViewById(R.id.create_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivityForResult(intent, REQ_CODE_CREATE);
            }
        });

        findViewById(R.id.edit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this,EditActivity.class);
                intent.putExtra(TICKET_KEY,tickets);
                startActivityForResult(intent,REQ_CODE_EDIT);

            }
        });

        findViewById(R.id.delete_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this,DeleteActivity.class);
                intent.putExtra(TICKET_KEY,tickets);
                startActivityForResult(intent,REQ_CODE_DELETE);
            }
        });

        findViewById(R.id.view_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(MainActivity.this,ViewActivity.class);
                intent.putExtra(TICKET_KEY,tickets);
                startActivity(intent);
            }
        });

        findViewById(R.id.finish_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.care_number).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:9999999999"));
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQ_CODE_CREATE){
            if (resultCode == RESULT_OK){
                ticket = new Ticket();
                ticket = (Ticket) data.getExtras().getSerializable(TICKET_KEY);
                Log.d("demo","in main"+ticket.getId());
                tickets.add(ticket);
            }
        }
        if (requestCode == REQ_CODE_EDIT){
            if(resultCode == RESULT_OK){
                ticket = new Ticket();
                ticket = (Ticket) data.getExtras().getSerializable(TICKET_KEY);
                index= isIdPresent(ticket.id);
                tickets.set(index, ticket);

            }
        }

        if(requestCode == REQ_CODE_DELETE){
            if(resultCode == RESULT_OK){
                ticket = new Ticket();
                int ticket_id = data.getExtras().getInt(TICKET_KEY);
                index = isIdPresent(ticket_id);
                tickets.remove(index);
            }
        }


    }

    public int isIdPresent(int id){
        int index=-1;
        for(int i=0;i<tickets.size();i++){
            if(tickets.get(i).getId()==id){
                index = i;
                break;
            }
        }
        return index;
    }
}
