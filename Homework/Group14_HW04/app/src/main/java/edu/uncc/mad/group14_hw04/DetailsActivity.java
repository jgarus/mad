package edu.uncc.mad.group14_hw04;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity implements GetMovieDetails.IDetail{
    final static String MOVIE_URL_KEY = "movie_url";
    ArrayList<Movie> movies_list =new ArrayList<Movie>();
    //Integer selected;
    TextView text;
    ImageView image;
    ImageView forward;
    ImageView backward;
    Button button;
    int index;
    RatingBar rate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        image = (ImageView) findViewById(R.id.image);
        index = getIntent().getExtras().getInt(SearchActivity.MOVIE_INDEX_KEY);
        movies_list = (ArrayList<Movie>) getIntent().getExtras().getSerializable(SearchActivity.MOVIES_KEY);
        new GetMovieDetails(this).execute("http://www.omdbapi.com");
    }

    @Override
    public void setDetails(ArrayList<Movie> movie) {
        movies_list=movie;
        Log.d("demo", String.valueOf(movie.size()));
        Log.d("demo",String.valueOf(index));
        forward = (ImageView) findViewById(R.id.forward_button);
        backward = (ImageView) findViewById(R.id.back_button);
        button = (Button) findViewById(R.id.button);
        setMovieDetails(index);
        forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                if (index == movies_list.size())
                    index = 0;
                setMovieDetails(index);
            }
        });

        backward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                if (index < 0)
                    index = movies_list.size() - 1;
                setMovieDetails(index);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsActivity.this, WebViewActivity.class);
                intent.putExtra(MOVIE_URL_KEY, image.getTag().toString());
                startActivity(intent);
            }
        });
    }

    @Override
    public ArrayList<Movie> getMovieList() {
        return movies_list;
    }

    public void setMovieDetails(int index){
        Movie movie = movies_list.get(index);
        image.setTag(movie.getId());
        new GetImage().execute(movie.getPoster());
        text = (TextView) findViewById(R.id.title_label);
        text.setText(movie.getTitle()+" ("+movie.getYear()+")");
        text = (TextView) findViewById(R.id.release_value);
        text.setText(movie.getReleased());
        text = (TextView) findViewById(R.id.genre_value);
        text.setText(movie.getGenre());
        text = (TextView) findViewById(R.id.director_value);
        text.setText(movie.getDirector());
        text = (TextView) findViewById(R.id.actor_value);
        text.setText(movie.getActors());
        text = (TextView) findViewById(R.id.plot_value);
        text.setText(movie.getPlot());
        rate = (RatingBar) findViewById(R.id.ratingBar);
        rate.setStepSize((float) 0.5);
        rate.setRating((float) ((movie.getRating() * 1.0) / 2.0));

    }

    class GetImage extends AsyncTask<String,Void,Bitmap>{

        @Override
        protected Bitmap doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                Bitmap bitmap = BitmapFactory.decodeStream(con.getInputStream());
                return bitmap;
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            image.setImageBitmap(bitmap);
        }
    }

}
