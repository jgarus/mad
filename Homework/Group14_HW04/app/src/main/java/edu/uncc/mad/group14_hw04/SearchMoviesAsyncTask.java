package edu.uncc.mad.group14_hw04;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by bradlamotte on 2/24/16.
 */
public class SearchMoviesAsyncTask extends AsyncTask<String, Void, ArrayList<Movie>> {
    String endpoint = "http://www.omdbapi.com/?type=movie&s=";
    ISearch activity;

    public SearchMoviesAsyncTask(ISearch activity){
        this.activity = activity;
    }

    @Override
    protected ArrayList<Movie> doInBackground(String... params) {
        try {
            URL url = new URL(endpoint + params[0]);
            Log.d("d", "search endpoint " + url.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();
            if(statusCode == HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line = reader.readLine();

                while(line != null){
                    builder.append(line);
                    line = reader.readLine();
                }

                return MovieUtil.SearchJsonParser.parse(builder.toString());
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        super.onPostExecute(movies);
        if(movies != null) {
            Collections.sort(movies, new MovieSorter());
        }
        //Log.d("d", "post execute movies " + movies.toString());
        this.activity.saveMovies(movies);
    }

    public interface ISearch{
        public void saveMovies(ArrayList<Movie> movies);
    }

    public class MovieSorter implements Comparator<Movie> {
        @Override
        public int compare(Movie m1, Movie m2) {
            if(m1.getYear() < m2.getYear()){
                return 1;
            } else {
                return -1;
            }
        }
    }
}
