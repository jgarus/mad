package edu.uncc.mad.group14_hw04;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class MovieUtil {

    static public class SearchJsonParser{
        static public ArrayList<Movie> parse(String input) throws JSONException {
            JSONObject root = new JSONObject(input);
            JSONArray moviesJSON = root.getJSONArray("Search");
            ArrayList<Movie> movies = new ArrayList<Movie>();

            for(int i=0; i<moviesJSON.length(); i++){
                JSONObject movieJSON = moviesJSON.getJSONObject(i);
                Movie movie = new Movie();
                movie.setTitle(movieJSON.getString("Title"));
                movie.setYear(movieJSON.getInt("Year"));
                movie.setId(movieJSON.getString("imdbID"));
                movie.setPoster(movieJSON.getString("Poster"));
                movies.add(movie);
            }

            return movies;
        }
    }

    static public class MovieDetailsParser{

        static Movie parseMovie(String in) throws JSONException {
            Movie movie;
            JSONObject movie_details = new JSONObject(in);
            movie = new Movie();
            movie.setTitle(movie_details.getString("Title"));
            movie.setYear(movie_details.getInt("Year"));
            movie.setId(movie_details.getString("imdbID"));
            movie.setActors(movie_details.getString("Actors"));
            movie.setDirector(movie_details.getString("Director"));
            movie.setGenre(movie_details.getString("Genre"));
            movie.setPlot(movie_details.getString("Plot"));
            movie.setPoster(movie_details.getString("Poster"));
            movie.setRating(movie_details.getInt("imdbRating"));
            movie.setReleased(movie_details.getString("Released"));
            return movie;
        }


    }
}
