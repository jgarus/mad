package edu.uncc.mad.group14_hw04;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import edu.uncc.mad.group14_hw04.Movie;
import edu.uncc.mad.group14_hw04.MovieUtil;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class GetMovieDetails extends AsyncTask<String,Void,ArrayList<Movie>> {
    IDetail Activity;
    ProgressDialog progressDialog;
    ArrayList<Movie> list;

    public GetMovieDetails(IDetail activity) {
        Activity = activity;
    }

    @Override
    protected ArrayList<Movie> doInBackground(String... params) {
        StringBuilder builder = new StringBuilder();
        BufferedReader reader;
        for(int i=0;i<list.size();i++) {
            builder.setLength(0);
            builder.append(params[0] + "/?i=" + list.get(i).getId());
            Log.d("demo",builder.toString());
            try {
                URL url = new URL(builder.toString());
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");
                con.connect();
                int statusCode = con.getResponseCode();
                if (statusCode == HttpURLConnection.HTTP_OK) {
                    reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    builder.setLength(0);
                    String line = reader.readLine();
                    while (line != null) {
                        builder.append(line);
                        line = reader.readLine();
                    }
                    list.set(i, MovieUtil.MovieDetailsParser.parseMovie(builder.toString()));
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    @Override
    protected void onPostExecute(ArrayList<Movie> movies) {
        super.onPostExecute(movies);
        progressDialog.dismiss();
        Activity.setDetails(movies);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog((Context) Activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading Movie");
        progressDialog.show();
        list = Activity.getMovieList();
    }



    public interface IDetail{
        void setDetails(ArrayList<Movie> movie);
        ArrayList<Movie> getMovieList();
    }

}
