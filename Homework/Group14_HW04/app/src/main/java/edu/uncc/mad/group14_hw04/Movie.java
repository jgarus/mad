package edu.uncc.mad.group14_hw04;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bradlamotte on 2/24/16.
 */
public class Movie implements Serializable {
    private String title, id, poster, genre, director, actors, plot;
    private Integer year;
    private Double rating;
    private Date released;

    public Movie(){
        this.title = null;
        this.id = null;
        this.poster = null;
        this.genre = null;
        this.director = null;
        this.actors = null;
        this.plot = null;
        this.year = null;
        this.rating = null;
        this.released = null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getReleased() {
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd yyyy");
        String date = null;
        if(released!=null) {
            date = formatter.format(released);
            return date;
        }
        else
            return "N/A";
    }

    public void setReleased(String date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        if(!date.equals("N/A")) {
            try {
                this.released = formatter.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else
            released = null;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", id='" + id + '\'' +
                ", poster='" + poster + '\'' +
                ", genre='" + genre + '\'' +
                ", director='" + director + '\'' +
                ", actors='" + actors + '\'' +
                ", plot='" + plot + '\'' +
                ", year=" + year +
                ", rating=" + rating +
                ", released=" + released +
                '}';
    }
}
