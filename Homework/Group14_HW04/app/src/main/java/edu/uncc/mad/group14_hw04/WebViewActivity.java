package edu.uncc.mad.group14_hw04;


import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends AppCompatActivity {
    WebView webview;
    String endpoint = "http://m.imdb.com/title/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        Log.d("d", "loading web view");

        webview = (WebView)findViewById(R.id.web_view);
        String id = (String) getIntent().getExtras().getString(DetailsActivity.MOVIE_URL_KEY);
        String url = endpoint + id;
        Log.d("d", "loading url " + url);
        webview.loadUrl(url);
        WebSettings settings = webview.getSettings();
        settings.setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient());
    }

}

