package edu.uncc.mad.group14_hw04;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements SearchMoviesAsyncTask.ISearch {
    ProgressDialog progress;
    ArrayList<Movie> movies;
    final static String MOVIES_KEY = "movies";
    final static String MOVIE_INDEX_KEY = "movie_index";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        showSearchingProgress();
        String searchTerm = getIntent().getExtras().getString(MainActivity.SEARCH_KEY);
        new SearchMoviesAsyncTask(SearchActivity.this).execute(searchTerm);
    }

    public void showSearchingProgress(){
        progress = new ProgressDialog(SearchActivity.this);
        progress.setMessage("Loading Movie List");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public void saveMovies(ArrayList<Movie> movies) {
        this.movies = movies;
        LinearLayout moviesView = (LinearLayout) findViewById(R.id.movies);

        if(movies != null) {
            Log.d("d", "saved movies " + movies.toString());
            Movie movie;

            for (int i = 0; i < movies.size(); i++) {
                movie = movies.get(i);
                moviesView.addView(createMovieView(movie, i));
            }
        } else {
            TextView tv = new TextView(SearchActivity.this);
            tv.setText("No movies found");
            moviesView.addView(tv);
        }

        progress.dismiss();
    }

    private void movieClicked(Integer movieIndex){
        Log.d("d", "movie clicked " + movieIndex);
        Intent intent = new Intent(SearchActivity.this, DetailsActivity.class);
        intent.putExtra(MOVIES_KEY, movies);
        intent.putExtra(MOVIE_INDEX_KEY, movieIndex);
        startActivity(intent);
    }

    private TextView createMovieView(Movie movie, int index){
        TextView movieView = new TextView(SearchActivity.this);
        movieView.setText(movie.getTitle() + " (" + movie.getYear() + ")");
        movieView.setTypeface(movieView.getTypeface(), Typeface.BOLD);
        movieView.setPadding(0, 50, 0, 50);
        movieView.setTag(index);

        movieView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieClicked(Integer.parseInt(v.getTag().toString()));
            }
        });

        return movieView;
    }
}
