// Assignment 03
// QuizActivity.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class QuizActivity extends AppCompatActivity implements GetImageAsyncTask.IImage {
    Integer questionId = 0;
    ArrayList<Question> questions;
    Integer scoreTotal = 0;
    final static String SCORE_KEY = "score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        try{
            questions = getIntent().getExtras().getParcelableArrayList(WelcomeActivity.QUESTIONS_KEY);
        } catch(NullPointerException e){
            Log.d("d", "No questions passed in");
        }

        findViewById(R.id.next_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerQuestion();
            }
        });

        findViewById(R.id.quit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(QuizActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        loadQuestion();
    }

    private void loadQuestion(){
        if(questionId <= (questions.size())) {
            Question question = questions.get(questionId);
            TextView label = (TextView) findViewById(R.id.question_label);
            label.setText("Q" + (question.getId() + 1));
            loadImage(question);
            TextView name = (TextView) findViewById(R.id.question_name);
            name.setText(question.getName());
            loadAnswers(question.getAnswers());
        }
    }

    private void loadAnswers(Answer[] answers){
        RadioGroup group = (RadioGroup) findViewById(R.id.answers_group);
        group.clearCheck();
        group.removeAllViews();
        RadioButton radio;

        // convert to arraylist
        ArrayList<Answer> answerArr = new ArrayList<Answer>(Arrays.asList(answers));
        Collections.shuffle(answerArr);

        for(Answer answer : answerArr){
            radio = new RadioButton(this);
            radio.setText(answer.getName());
            radio.setId(answer.getScore());
            group.addView(radio);
        }
    }

    private void answerQuestion(){
        RadioGroup group = (RadioGroup) findViewById(R.id.answers_group);
        int score = group.getCheckedRadioButtonId();

        if(score < 0){
            Toast.makeText(QuizActivity.this, "Please choose an answer", Toast.LENGTH_SHORT).show();
        } else {
            scoreTotal += score;

            if (questionId == (questions.size() - 1)) {
                sendToResult();
            } else {
                questionId++;
                loadQuestion();
            }
        }
    }

    private void sendToResult(){
        Intent intent = new Intent(QuizActivity.this, ResultActivity.class);
        intent.putExtra(SCORE_KEY, scoreTotal);
        startActivity(intent);
        finish();
    }

    private void loadImage(Question question){
        ProgressBar progress = (ProgressBar) findViewById(R.id.image_progress);
        progress.setVisibility(View.GONE);
        ImageView image = (ImageView) findViewById(R.id.question_image);
        image.setImageBitmap(null);
        image.setVisibility(View.GONE);

        if(question.getImage() != null){
            progress.setVisibility(View.VISIBLE);
            new GetImageAsyncTask(this).execute(question.getImage());
        }
    }

    @Override
    public void displayImage(Bitmap bm) {
        ImageView image = (ImageView) findViewById(R.id.question_image);
        ProgressBar progress = (ProgressBar) findViewById(R.id.image_progress);
        progress.setVisibility(View.GONE);
        image.setImageBitmap(bm);
        image.setVisibility(View.VISIBLE);
    }
}
