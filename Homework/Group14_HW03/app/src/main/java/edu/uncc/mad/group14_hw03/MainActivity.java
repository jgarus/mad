// Assignment 03
// MainActivity.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    private int SPLASH_LENGTH = 8000;
    Thread splashScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create splash screen thread
        splashScreen = new Thread(){
            @Override
            public void run(){
                //make splash last the allotted time
                try{
                    //only this thread can access
                    synchronized (this){
                        //waits thread
                        wait(SPLASH_LENGTH);
                    }
                }catch(InterruptedException e){

                }
                finally {
                    if(isFinishing()==false)//if we haven't called finish then...
                    {
                        Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        };
        splashScreen.start();

        //start the welcome activity using the button
        findViewById(R.id.start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
                startActivity(intent);
            }
        });
    }

    //pause thread
    @Override
    public void onPause(){
        super.onPause();
        //if thread state is waiting, then finish the activity
        if(splashScreen.getState()==Thread.State.TIMED_WAITING){
            finish();
        }
    }
}



