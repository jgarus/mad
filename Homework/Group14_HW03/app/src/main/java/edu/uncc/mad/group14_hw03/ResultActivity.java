// Assignment 03
// ResultActivity.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultActivity extends AppCompatActivity implements GetQuestionsAsyncTask.IQuestions {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        findViewById(R.id.button_quit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ResultActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }
        });

        findViewById(R.id.button_try).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProgressDialog progress = new ProgressDialog(ResultActivity.this);
                progress.setMessage("Loading Questions");
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setCancelable(false);
                progress.show();
                new GetQuestionsAsyncTask(ResultActivity.this).execute();
            }
        });

        ArrayList<Result> results = Result.getResults();
        int score = getIntent().getExtras().getInt(QuizActivity.SCORE_KEY);
        Log.d("d", "scoreon result: " + score);

        for(Result result : results){
            if(result.match(score)){
                showResult(result);
                break;
            }
        }
    }

    private void showResult(Result result){
        Log.d("d", "result: " + result.toString());
        TextView name = (TextView) findViewById(R.id.result_display);
        name.setText(result.getName());
        ImageView image = (ImageView) findViewById(R.id.result_image);
        image.setImageResource(result.getImage());
        TextView description = (TextView) findViewById(R.id.description);
        description.setText(result.getDescription());
    }

    @Override
    public void saveQuestions(ArrayList<Question> questions) {
        Intent intent = new Intent(ResultActivity.this, QuizActivity.class);
        intent.putParcelableArrayListExtra(WelcomeActivity.QUESTIONS_KEY, questions);
        startActivity(intent);
        finish();
    }
}
