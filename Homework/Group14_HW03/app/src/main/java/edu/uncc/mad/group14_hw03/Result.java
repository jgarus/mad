// Assignment 03
// Result.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import java.util.ArrayList;

/**
 * Created by bradlamotte on 2/19/16.
 */
public class Result {
    private int floor, ceiling, image;
    private String name, description;

    public int getFloor() {
        return floor;
    }

    public Result(int floor, int ceiling, String name, String description, int image) {
        this.floor = floor;
        this.ceiling = ceiling;
        this.name = name;
        this.description = description;
        this.image = image;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getCeiling() {
        return ceiling;
    }

    public void setCeiling(int ceiling) {
        this.ceiling = ceiling;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public Boolean match(int score){
        return (score >= floor && score <= ceiling);
    }

    @Override
    public String toString() {
        return "Result{" +
                "floor=" + floor +
                ", ceiling=" + ceiling +
                ", image=" + image +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    public static ArrayList<Result> getResults(){
        ArrayList<Result> results = new ArrayList<Result>();
        results.add(new Result(0, 10, "Non-Geek", "There isn't a single geeky bone in your body. You prefer to party rather than study, and have someone else fix your computer, if need be. You're just too cool for this. You probably don't even wear glasses!", R.drawable.nongeek));
        results.add(new Result(11, 50, "Semi-Geek", "Maybe you're just influenced by the trend, or maybe you just got it all perfectly balanced. You have some geeky traits, but they aren't as \"hardcore\" and they don't take over your\n life. You like some geeky things, but aren't nearly as obsessive about them as the uber-geeks. You actually get to enjoy both worlds", R.drawable.semi_geek));
        results.add(new Result(51, 72, "Uber-Geek", "You are the geek supreme! You are likely to be interested in technology, science, gaming and geeky media such as Sci-Fi and fantasy. All the mean kids that used to laugh at you in high school are now begging you for a job. Be proud of your geeky nature, for geeks shall inherit the Earth!", R.drawable.uber_geek));
        return results;
    }
}
