// Assignment 03
// WelcomActivity.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Arrays;

public class WelcomeActivity extends AppCompatActivity implements GetQuestionsAsyncTask.IQuestions {
    ProgressDialog progress;
    ArrayList<Question> questions;
    final static String QUESTIONS_KEY = "questions";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        findViewById(R.id.exit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        findViewById(R.id.start_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WelcomeActivity.this, QuizActivity.class);
                intent.putParcelableArrayListExtra(QUESTIONS_KEY, questions);
                startActivity(intent);
                finish();
            }
        });

        showProgress();
        new GetQuestionsAsyncTask(WelcomeActivity.this).execute();
    }

    private void showProgress(){
        progress = new ProgressDialog(WelcomeActivity.this);
        progress.setMessage("Loading Questions");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public void saveQuestions(ArrayList<Question> questions) {
        this.questions = questions;
        Button startButton = (Button) findViewById(R.id.start_button);
        startButton.setEnabled(true);
        progress.dismiss();
    }
}
