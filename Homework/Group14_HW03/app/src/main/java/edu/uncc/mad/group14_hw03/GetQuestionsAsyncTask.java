// Assignment 03
// GetQuestionsAsyncTask.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

import static java.lang.Integer.parseInt;

/**
 * Created by bradlamotte on 2/18/16.
 */
public class GetQuestionsAsyncTask extends AsyncTask<Void, Void, ArrayList<Question>> {
    final static private String questionsUrl = "http://dev.theappsdr.com/apis/spring_2016/hw3/index.php?qid=<id>";
    IQuestions activity;

    public GetQuestionsAsyncTask(IQuestions activity){
        this.activity = activity;
    }

    @Override
    protected ArrayList<Question> doInBackground(Void... params) {
        ArrayList<Question> questions = new ArrayList<>();
        for(int i=0; i<7; i++){
            questions.add(getQuestion(i));
        }
        return questions;
    }

    @Override
    protected void onPostExecute(ArrayList<Question> questions) {
        super.onPostExecute(questions);
        this.activity.saveQuestions(questions);
    }

    private Question getQuestion(int questionId){
        String thisQuestionUrl = questionsUrl.replace("<id>", questionId + "");
        BufferedReader reader = null;

        try {
            URL url = new URL(thisQuestionUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
            return parseQuestion(sb.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private Question parseQuestion(String contents){
        String[] questionPieces = contents.split(";");
        Question question = new Question();
        question.setId(parseInt(questionPieces[0]));
        question.setName(questionPieces[1].trim());
        String[] answerPieces;

        if((questionPieces.length % 2) != 0){
            question.setImage(questionPieces[questionPieces.length - 1].trim());
            answerPieces = Arrays.copyOfRange(questionPieces, 2, (questionPieces.length - 1));
        } else {
            answerPieces = Arrays.copyOfRange(questionPieces, 2, questionPieces.length);
        }

        question.setAnswers(parseAnswers(answerPieces));
        return question;
    }

    public interface IQuestions{
        public void saveQuestions(ArrayList<Question> questions);
    }

    private Answer[] parseAnswers(String[] pieces){
        ArrayList<Answer> answers = new ArrayList<Answer>();

        for (int i = 0; i < pieces.length; i += 2){
            Answer answer = new Answer();
            answer.setName(pieces[i].trim());
            answer.setScore(parseInt(pieces[i + 1].trim()));
            answers.add(answer);
        }

        return answers.toArray(new Answer[answers.size()]);
    }
}
