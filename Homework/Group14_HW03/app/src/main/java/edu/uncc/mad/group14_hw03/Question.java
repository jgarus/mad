// Assignment 03
// Question.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by bradlamotte on 2/18/16.
 */
public class Question implements Parcelable {
    private int id;
    private String name, image;
    private Answer[] answers;

    public Question(){
        this.answers = new Answer[0];
    }

    public Question(int id, String name, String image, Answer[] answers) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.answers = answers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Answer[] getAnswers() {
        return answers;
    }

    public void setAnswers(Answer[] answers) {
        this.answers = answers;
    }

    @Override
    public String toString() {
        return "Question{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
        dest.writeParcelableArray(answers, 0);
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>(){
        public Question createFromParcel(Parcel in){
            return new Question(in);
        }
        public Question[] newArray(int size){
            return new Question[size];
        }
    };

    private Question(Parcel in){
        this.setId(in.readInt());
        this.setName(in.readString());
        this.setImage(in.readString());
        Parcelable[] parcels = in.readParcelableArray(Answer.class.getClassLoader());
        Answer[] answers = new Answer[parcels.length];

        for(int i=0; i<parcels.length; i++){
            answers[i] = (Answer) parcels[i];
        }

        this.setAnswers(answers);
    }
}
