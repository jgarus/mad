// Assignment 03
// Answer.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by bradlamotte on 2/18/16.
 */
public class Answer implements Parcelable {
    private int score;
    private String name;

    public Answer(){
    }

    public Answer(String name, int score) {
        this.score = score;
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Answer{" +
                "score=" + score +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(score);
    }

    public static final Parcelable.Creator<Answer> CREATOR = new Parcelable.Creator<Answer>(){
        public Answer createFromParcel(Parcel in){
            return new Answer(in);
        }
        public Answer[] newArray(int size){
            return new Answer[size];
        }
    };

    private Answer(Parcel in){
        this.setName(in.readString());
        this.setScore(in.readInt());
    }
}
