// Assignment 03
// GetImageAsyncTask.java
// Brad LaMotte, Jesus Garcia, Ahyuta Ram Rampalli

package edu.uncc.mad.group14_hw03;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by bradlamotte on 2/19/16.
 */
public class GetImageAsyncTask extends AsyncTask<String, Void, Bitmap> {
    IImage activity;

    public GetImageAsyncTask(IImage activity) {
        this.activity = activity;
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            Bitmap image = BitmapFactory.decodeStream(con.getInputStream());
            return image;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bm) {
        this.activity.displayImage(bm);
        super.onPostExecute(bm);
    }

    public interface IImage{
        public void displayImage(Bitmap bm);
    }
}
