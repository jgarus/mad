package edu.uncc.mad.group14_inclass06;

import android.os.AsyncTask;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 2/22/16.
 */

public class GetStoriesAsynTask extends AsyncTask<String, Void, ArrayList<Story>> {
    IStories activity;

    public GetStoriesAsynTask(IStories activity){
        this.activity = activity;
    }

    @Override
    protected ArrayList<Story> doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            Log.d("d", "url " + url.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();

            Log.d("d", "statusCode: " + statusCode + ", should be: " + HttpURLConnection.HTTP_OK);

            if(statusCode == HttpURLConnection.HTTP_OK){
                InputStream in = con.getInputStream();

                // SAX Parser
                // return PersonUtil.PersonsSAXParser.parsePerson(in);

                // Pull Parser
                return NewsUtil.NewsPullParser.parseStories(in);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Story> stories) {
        super.onPostExecute(stories);
        if(stories != null) {
            Log.d("d", "parsed stories: " + stories.toString());
            this.activity.saveStories(stories);
        }
    }

    public interface IStories{
        public void saveStories(ArrayList<Story> stories);
    }
}
