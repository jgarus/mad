package edu.uncc.mad.group14_inclass06;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;

public class NewsDetailsActivity extends AppCompatActivity implements GetImageAsyncTask.Iimage {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        final Story story = (Story) getIntent().getExtras().getSerializable(StoriesActivity.STORY_KEY);

        findViewById(R.id.imageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewsDetailsActivity.this, NewsWebViewActivity.class);
                intent.putExtra("url", story.getLink());
                startActivity(intent);
            }
        });


        Log.d("d", "story details" + story.toString());
        displayStoryDetail(story);
        new GetImageAsyncTask(NewsDetailsActivity.this).execute(story.getImage());
    }

    private void displayStoryDetail(Story story) {
        TextView title = (TextView) findViewById(R.id.title);
        title.setText(story.getTitle());
        TextView pubDate = (TextView) findViewById(R.id.pubDate);
        pubDate.setText(story.getDate());
        TextView description = (TextView) findViewById(R.id.description);
        description.setText((Html.fromHtml(story.getDescription())));
    }

    @Override
    public void displayImage(Bitmap bm) {
        ImageView image = (ImageView) findViewById(R.id.imageView);
        image.setImageBitmap(bm);
    }
}
