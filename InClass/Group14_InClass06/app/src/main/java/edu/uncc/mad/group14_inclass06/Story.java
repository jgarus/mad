package edu.uncc.mad.group14_inclass06;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jesus on 2/22/16.
 */
public class Story implements Serializable {

    private String title, description, link, image;
    Date pubDate;

    public Story(){}

    public Story(String title, String description, Date pubDate, String link, String image){
        this.title = title;
        this.description = description;
        this.pubDate = pubDate;
        this.link = link;
        this.image = image;
    }


    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }


    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDate(){
        //MM/DD/YYYY HH:MM AM/ PM
        //SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat dateFormat = new SimpleDateFormat("M/dd/yyyy h:MM a");
        return dateFormat.format(this.pubDate);
    }

    public void setDate(String pubDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        try {
            this.pubDate = dateFormat.parse(pubDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public String getLink(){
        return link;
    }

    public void setLink(String link){
        this.link = link;
    }

    public String getImage(){
        return image;
    }

    public void setImage(String image){
        this.image = image;
    }

    public String toString() {
        return "Story{" +
                " Title: " + title +
                " Desctiption: " + description +
                " Publication Date: " + pubDate +
                " Link: " + link +
                " Thumbnail: " + image +
                "}";
    }


}
