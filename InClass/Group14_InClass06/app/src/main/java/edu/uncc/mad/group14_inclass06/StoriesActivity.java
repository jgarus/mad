package edu.uncc.mad.group14_inclass06;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class StoriesActivity extends AppCompatActivity implements GetStoriesAsynTask.IStories {
    final static String STORY_KEY = "story";
    ProgressDialog progress;
    ArrayList<Story> stories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        String storiesUrl = getIntent().getExtras().getString(MainActivity.STORIES_KEY);
        Log.d("d", "stories url " + storiesUrl);

        showGetStoriesProgress();
        new GetStoriesAsynTask(StoriesActivity.this).execute(storiesUrl);
    }

    @Override
    public void saveStories(ArrayList<Story> stories) {
        Log.d("d", "saved stories " + stories.toString());
        Collections.sort(stories, new StorySorter());
        this.stories = stories;
        LinearLayout container = (LinearLayout) findViewById(R.id.stories);
        TextView storyDisplay;
        Story story;
        int storyIndex;

        for(int i=0; i<stories.size(); i++){
            story = stories.get(i);
            storyDisplay = new TextView(StoriesActivity.this);
            storyDisplay.setText(story.getTitle());
            storyDisplay.setTypeface(storyDisplay.getTypeface(), Typeface.BOLD);
            storyDisplay.setPadding(0, 0, 0, 20);
            storyDisplay.setTag(i);
            storyDisplay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    storyClicked(Integer.parseInt(v.getTag().toString()));
                }
            });
            container.addView(storyDisplay);
        }
        progress.dismiss();
    }

    public void showGetStoriesProgress(){
        progress = new ProgressDialog(StoriesActivity.this);
        progress.setMessage("Loading News");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    private void storyClicked(Integer storyIndex){
        Log.d("d", "story clicked " + storyIndex);
        Log.d("d", "story: " + stories.get(storyIndex).toString());
        Story story = stories.get(storyIndex);
        Intent intent = new Intent(StoriesActivity.this, NewsDetailsActivity.class);
        intent.putExtra(STORY_KEY, story);
        startActivity(intent);
    }

    public class StorySorter implements Comparator<Story> {
        @Override
        public int compare(Story s1, Story s2) {
            return s2.getDate().compareTo(s1.getDate());
        }
    }
}
