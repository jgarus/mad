package edu.uncc.mad.group14_inclass06;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class NewsWebViewActivity extends AppCompatActivity {
    WebView web;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_web_view);

        web = (WebView) findViewById(R.id.webView);
        web.loadUrl((String) getIntent().getExtras().get("url"));
        WebSettings settings = web.getSettings();
        settings.setJavaScriptEnabled(true);
        web.setWebViewClient(new WebViewClient());
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        return super.onKeyDown(keyCode, event);
////        if ((keyCode == KeyEvent.KEYCODE_BACK) && web.canGoBack()) {
////            //web.goBack();
////            return true;
////        }
//        return super.onKeyDown(keyCode, event);
//    }
}
