package edu.uncc.mad.group14_inclass06;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    final static String STORIES_KEY = "stories";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void getView(View v){
        Intent intent = new Intent(MainActivity.this, StoriesActivity.class);
        intent.putExtra(STORIES_KEY, v.getTag().toString());
        startActivity(intent);
    }
}
