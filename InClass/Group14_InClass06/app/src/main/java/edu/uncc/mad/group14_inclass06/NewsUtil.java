package edu.uncc.mad.group14_inclass06;

/**
 * Created by bradlamotte on 2/22/16.
 */

import android.util.Xml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import android.util.Log;
import android.util.Xml;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 2/17/16.
 */
public class NewsUtil {

   static public class NewsPullParser{

        static public ArrayList<Story> parseStories(InputStream input) throws XmlPullParserException, IOException {
            XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
            parser.setInput(input, "UTF-8");

            ArrayList<Story> stories = new ArrayList<Story>();
            Story story = null;
            int event = parser.getEventType();
            boolean isItem = false;

            while(event != XmlPullParser.END_DOCUMENT){
                switch (event){
                    case XmlPullParser.START_TAG:
                        switch(parser.getName()){
                            case "item":
                                isItem = true;
                                story = new Story();
                                break;
                            case "title":
                                Log.d("d", "depth " + parser.getDepth());
                                if(isItem) {
                                    story.setTitle(parser.nextText().trim());
                                }
                                break;
                            case "link":
                                if(isItem) {
                                    story.setLink(parser.nextText().trim());
                                }
                                break;
                            case "pubDate":
                                if(isItem) {
                                    story.setDate(parser.nextText().trim());
                                }
                                break;
                            case "description":
                                if(isItem) {
                                    story.setDescription(parser.nextText().trim());
                                }
                                break;
                            case "media:thumbnail":
                                if(isItem) {
                                    story.setImage(parser.getAttributeValue(null, "url").trim());
                                }
                                break;
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        if(parser.getName().equals("item")) {
                            stories.add(story);
                            isItem = false;
                        }
                        break;
                }
                event = parser.next();
            }

            return stories;
        }
    }
}
