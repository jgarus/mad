package edu.uncc.mad.group14_inclass06;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class GetImageAsyncTask extends AsyncTask<String, Void, Bitmap> {
    Iimage activity;

    public GetImageAsyncTask(Iimage activity) {
        this.activity = activity;
    }
    @Override
    protected Bitmap doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            Bitmap image = BitmapFactory.decodeStream(con.getInputStream());
            return image;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Bitmap bm) {
        this.activity.displayImage(bm);
        super.onPostExecute(bm);
    }

    public interface Iimage{
        public void displayImage(Bitmap bm);
    }

}
