// In Class 05
// MainActivity.java
// Brad LaMotte
// 800881564

package edu.uncc.mad.inclass05_800881564;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements GetImages.IImages, GetImage.IImage {
    ProgressDialog progress;
    HashMap<String, ArrayList<String>> imageDictionary = new HashMap<>();
    ArrayList<String> images = new ArrayList<>();
    Integer imageIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.go_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView image = (ImageView) findViewById(R.id.imageDisplay);
                image.setImageBitmap(null);
                showButtons(false);

                if (isConnectedOnline()) {
                    EditText keywordField = (EditText) findViewById(R.id.keywords);
                    String keywords = keywordField.getText().toString();

                    if (keywords != null && !keywords.isEmpty()) {
                        searchImages(keywords);
                    } else {
                        Toast.makeText(MainActivity.this, "Enter a keyword", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No network!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.previous).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageIndex--;
                showImage();
            }
        });

        findViewById(R.id.next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageIndex++;
                showImage();
            }
        });

        if (isConnectedOnline()) {
            showGetImagesProgress();
            new GetImages(MainActivity.this).execute();
        } else {
            Toast.makeText(MainActivity.this, "No network!", Toast.LENGTH_SHORT).show();
        }
    }

    public void showGetImagesProgress(){
        progress = new ProgressDialog(MainActivity.this);
        progress.setMessage("Loading dictionary");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    private boolean isConnectedOnline(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        if(info != null && info.isConnected()){
            return true;
        } else {
            return false;
        }
    }

    public void saveDictionary(HashMap<String, ArrayList<String>> dict){
        Log.d("d", "saving dictionary");
        Log.d("d", "returned dictionary: " + dict.toString());
        this.imageDictionary = dict;
        progress.dismiss();
    }

    public void showImage(){
        if(images.size() == 0){
            Toast.makeText(MainActivity.this, "No images found", Toast.LENGTH_SHORT).show();
        } else {
            if((imageIndex+1) > images.size()){
                imageIndex = 0;
            } else if((imageIndex) < 0){
                imageIndex = images.size() - 1;
            }

            progress = new ProgressDialog(MainActivity.this);
            progress.setMessage("Loading image");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setCancelable(false);
            progress.show();

            Log.d("d", "showing image");
            Log.d("d", "image index " + imageIndex);
            String imageUrl = images.get(imageIndex);
            Log.d("d", imageUrl);

            if(imageUrl != null && !imageUrl.isEmpty()) {
                new GetImage(MainActivity.this).execute(imageUrl);
            }
        }
    }

    public void displayImage(Bitmap bm){
        progress.dismiss();
        Log.d("d", "displaying Image");
        ImageView image = (ImageView) findViewById(R.id.imageDisplay);
        image.setImageBitmap(bm);
        if(images.size() > 1){
            showButtons(true);
        }
    }

    public void showButtons(Boolean show){
        ImageView previous = (ImageView) findViewById(R.id.previous);
        ImageView next = (ImageView) findViewById(R.id.next);

        if(show){
            previous.setVisibility(View.VISIBLE);
            next.setVisibility(View.VISIBLE);
        } else {
            previous.setVisibility(View.INVISIBLE);
            next.setVisibility(View.INVISIBLE);
        }
    }

    void searchImages(String keywords){
        if(imageDictionary.containsKey(keywords)){
            images = imageDictionary.get(keywords);
        } else {
            images = new ArrayList<>();
        }
        showImage();
    }
}
