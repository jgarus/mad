// In Class 05
// GetImages.java
// Brad LaMotte
// 800881564

package edu.uncc.mad.inclass05_800881564;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class GetImages extends AsyncTask<Void, Void, HashMap<String, ArrayList<String>>> {
    IImages activity;
    final static String sourceUrl = "http://dev.theappsdr.com/apis/spring_2016/inclass5/URLs.txt";
    String keywords;

    public GetImages(IImages activity) {
        this.activity = activity;
        this.keywords = keywords;
    }

    @Override
    protected HashMap<String, ArrayList<String>> doInBackground(Void... params) {
        BufferedReader reader = null;
        HashMap<String, ArrayList<String>> dictionary = new HashMap<>();

        try {
            URL url = new URL(sourceUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            StringBuilder sb = new StringBuilder();
            String line = "";
            while((line = reader.readLine()) != null){
                sb.append(line + "\n");
            }
            dictionary = parseContents(dictionary, sb.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return dictionary;
    }

    @Override
    protected void onPostExecute(HashMap<String, ArrayList<String>> dict) {
        this.activity.saveDictionary(dict);
        super.onPostExecute(dict);
    }

    public interface IImages{
        public void saveDictionary(HashMap<String, ArrayList<String>> images);
    }

    private HashMap<String, ArrayList<String>> parseContents(HashMap<String, ArrayList<String>> dict, String contents){
        String[] contArr = contents.split(";");
        String[] j;

        for(String i : contArr){
            i = i.trim();

            if(i != null && i.length() > 0){
                Log.d("d", "i " + i);
                j = i.split(",");

                Log.d("d", "k " + j[0]);

                if(!dict.containsKey(j[0])){
                    dict.put(j[0], new ArrayList<String>());
                }

                dict.get(j[0]).add(j[1]);

//                if(j[0].equals(this.keywords)){
//                    Log.d("d", "found");
//                    images.add(j[1]);
//                }
            }
        }

        return dict;
    }
}

