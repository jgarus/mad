package edu.uncc.mad.group14_inclass08;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class StoriesActivity extends AppCompatActivity implements GetStoriesAsynTask.IStories, GetBookmarksAsyncTask.IStories {
    final static String STORY_KEY = "story";
    final static String BOOKMARK_KEY = "bookmark";
    ProgressDialog progress;
    DataManager dm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        showGetStoriesProgress();

        try{
            String section = getIntent().getExtras().getString(MainActivity.SECTION_KEY);
            new GetStoriesAsynTask(StoriesActivity.this).execute(section);
        } catch(Exception e){
            new GetBookmarksAsyncTask(StoriesActivity.this, StoriesActivity.this).execute();
        }
    }

    @Override
    public void saveStories(final ArrayList<Story> stories) {
        //Log.d("d", "saved stories " + stories.toString());
        final ArrayList<Story> storiesNew;
        storiesNew = stories;

        ListView listView = (ListView) findViewById(R.id.listView);
        StoryAdapter adapter = new StoryAdapter(this, R.layout.story_view, stories);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("d", "position: " + position);
                Story story = (Story) storiesNew.get(position);
                Intent intent = new Intent(StoriesActivity.this, NewsDetailsActivity.class);
                intent.putExtra(STORY_KEY, story);

                //ImageView bookmark = (ImageView) view.findViewById(R.id.bookmark);
                dm = new DataManager(StoriesActivity.this);
                Story clicked = dm.getNote(story.getUrl());

                //Log.d("d", "clicked " + clicked.toString());
                if(clicked == null){
                    intent.putExtra(BOOKMARK_KEY, R.drawable.bookmark_empty);
                } else {
                    intent.putExtra(BOOKMARK_KEY, R.drawable.bookmark_filled);
                }

                startActivity(intent);
            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("d", "long clicked");
                dm = new DataManager(StoriesActivity.this);
                ImageView bookmark = (ImageView) view.findViewById(R.id.bookmark);
                Story clicked = stories.get(position);
                Story story = dm.getNote(clicked.getUrl());

                if(story == null){
                    Log.d("d", "bookmark");
                    dm.saveNote(clicked);
                    bookmark.setImageResource(R.drawable.bookmark_filled);
                } else {
                    Log.d("d", "unbookmark");
                    dm.deleteNOte(clicked);
                    bookmark.setImageResource(R.drawable.bookmark_empty);
                }
                return true;
            }
        });

        progress.dismiss();
    }

    public void showGetStoriesProgress(){
        progress = new ProgressDialog(StoriesActivity.this);
        progress.setMessage("Loading Stories");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_news, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            case R.id.bookmark_menu:
                intent = new Intent(StoriesActivity.this, StoriesActivity.class);
                startActivity(intent);
                finish();

                return true;
            case R.id.deletebk_menu:
                dm = new DataManager(StoriesActivity.this);
                dm.deleteAll();
                intent = new Intent(StoriesActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
