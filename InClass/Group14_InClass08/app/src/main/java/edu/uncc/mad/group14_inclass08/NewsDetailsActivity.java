package edu.uncc.mad.group14_inclass08;

import android.media.Image;
import android.widget.ImageView;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

//
public class NewsDetailsActivity extends AppCompatActivity {
//
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);

        final Story story = (Story) getIntent().getExtras().getSerializable(StoriesActivity.STORY_KEY);

        Log.d("d", "story details " + story.toString());
        displayStoryDetail(story);
    }

    private void displayStoryDetail(Story story) {
        TextView title = (TextView) findViewById(R.id.storytitle_label);
        title.setText(story.getTitle());
        TextView byline = (TextView) findViewById(R.id.storylinetitle_label);
        byline.setText(story.getByline());
        ImageView image = (ImageView) findViewById(R.id.image_details);
        Picasso.with(this).
                load(story.getImageUrl()).
                //resize(250, 250).
                into(image);
        Log.d("d", "image url " + story.getImageUrl());
        TextView abst = (TextView) findViewById(R.id.abstract_value);
        abst.setText((Html.fromHtml(story.getAbstr())));

        ImageView bookmark = (ImageView) findViewById(R.id.bookmark);
        int bookmarkId = getIntent().getExtras().getInt(StoriesActivity.BOOKMARK_KEY);
        Log.d("d", "bookmakr id " + bookmarkId);
        bookmark.setImageResource(bookmarkId);
    }


}
