package edu.uncc.mad.group14_inclass08;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by bradlamotte on 3/14/16.
 */
public class StoriesTable {
    static final String TABLE_NAME = "stories";
    //static final String COLUMN_ID = "id";
    static final String COLUMN_URL = "url";
    static final String COLUMN_TITLE = "title";
    static final String COLUMN_BYLINE = "byline";
    static final String COLUMN_ABSTRACT = "abstract";
    static final String COLUMN_CREATED = "created";
    static final String COLUMN_THUMB = "thumb";
    static final String COLUMN_IMAGE = "image";
    static final String COLUMN_SECTION = "section";


    static public void onCreate(SQLiteDatabase db){
        StringBuilder sb = new StringBuilder();
        sb.append("create table " + TABLE_NAME + " (");
        sb.append(COLUMN_URL + " text primary key not null, ");
        sb.append(COLUMN_TITLE + " text not null, ");
        sb.append(COLUMN_BYLINE + " text null, ");
        sb.append(COLUMN_ABSTRACT + " text null, ");
        sb.append(COLUMN_CREATED + " datetime not null, ");
        sb.append(COLUMN_THUMB + " text null, ");
        sb.append(COLUMN_IMAGE + " text null, ");
        sb.append(COLUMN_SECTION + " text null);");

        try{
            db.execSQL(sb.toString());
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    static public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("drop table if exists " + TABLE_NAME);
        StoriesTable.onCreate(db);
    }
}
