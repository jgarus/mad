package edu.uncc.mad.group14_inclass08;

import android.content.Context;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;


import java.util.List;

/**
 * Created by bradlamotte on 2/29/16.
 */
public class StoryAdapter extends ArrayAdapter<Story> {

    List<Story> stories;
    Context context;
    int resource;
    DataManager dm;

    public StoryAdapter(Context context, int resource, List<Story> stories) {
        super(context, resource, stories);
        this.stories = stories;
        this.context = context;
        this.resource = resource;
        this.dm = new DataManager(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(this.resource, parent, false);
        }

        Story story = this.stories.get(position);
        Log.d("d", "story " + story.toString());
        TextView title = (TextView) convertView.findViewById(R.id.title);
        title.setText(story.getTitle());
        TextView date = (TextView) convertView.findViewById(R.id.date);
        date.setText(story.getCreated());
        ImageView image = (ImageView) convertView.findViewById(R.id.image);

        ImageView bookmark = (ImageView) convertView.findViewById(R.id.bookmark);


        Story bookmarkedStory = this.dm.getNote(story.getUrl());
        int bookmarkResource;

        if(bookmarkedStory != null){
            bookmarkResource = R.drawable.bookmark_filled;
        } else{
            bookmarkResource = R.drawable.bookmark_empty;
        }
        bookmark.setImageResource(bookmarkResource);

        Log.d("d", "title " + story.getTitle());
        Log.d("d", "story thumb " + story.getThumbnailUrl());
        Picasso.with(this.context).
                load(story.getThumbnailUrl()).
                resize(75, 75).
                into(image);

        return convertView;
    }
}
