package edu.uncc.mad.group14_inclass08;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/14/16.
 */
public class StoryDAO {
    private SQLiteDatabase db;

    public StoryDAO(SQLiteDatabase db) {
        this.db = db;
    }

    public long save(Story story){
        ContentValues values = new ContentValues();
        values.put(StoriesTable.COLUMN_URL, story.getUrl());
        values.put(StoriesTable.COLUMN_TITLE, story.getTitle());
        values.put(StoriesTable.COLUMN_BYLINE, story.getByline());
        values.put(StoriesTable.COLUMN_ABSTRACT, story.getAbstr());
        values.put(StoriesTable.COLUMN_CREATED, story.getActualCreated().toString());
        values.put(StoriesTable.COLUMN_THUMB, story.getThumbnailUrl());
        values.put(StoriesTable.COLUMN_IMAGE, story.getImageUrl());
        values.put(StoriesTable.COLUMN_SECTION, story.getSection());
        return db.insert(StoriesTable.TABLE_NAME, null, values);
    }

    public boolean update(Story story){
        ContentValues values = new ContentValues();
        values.put(StoriesTable.COLUMN_URL, story.getUrl());
        values.put(StoriesTable.COLUMN_TITLE, story.getTitle());
        values.put(StoriesTable.COLUMN_BYLINE, story.getByline());
        values.put(StoriesTable.COLUMN_ABSTRACT, story.getAbstr());
        values.put(StoriesTable.COLUMN_CREATED, story.getActualCreated().toString());
        values.put(StoriesTable.COLUMN_THUMB, story.getThumbnailUrl());
        values.put(StoriesTable.COLUMN_IMAGE, story.getImageUrl());
        values.put(StoriesTable.COLUMN_SECTION, story.getSection());
        return db.update(StoriesTable.TABLE_NAME, values, StoriesTable.COLUMN_URL + "=?", new String[]{story.getUrl()}) > 0;
    }

    public boolean delete(Story story){
        return db.delete(StoriesTable.TABLE_NAME, StoriesTable.COLUMN_URL + "=?", new String[]{story.getUrl()}) > 0;
    }

    public Story get(String url){
        Story story = null;

        Cursor c = db.query(true, StoriesTable.TABLE_NAME, new String[]{
                StoriesTable.COLUMN_URL,
                StoriesTable.COLUMN_TITLE,
                StoriesTable.COLUMN_BYLINE,
                StoriesTable.COLUMN_ABSTRACT,
                StoriesTable.COLUMN_CREATED,
                StoriesTable.COLUMN_THUMB,
                StoriesTable.COLUMN_IMAGE,
                StoriesTable.COLUMN_SECTION}, StoriesTable.COLUMN_URL + "=?", new String[]{url}, null, null, null, null, null);

        if(c != null && c.moveToFirst()){
            story = buildNoteFromCursor(c);
            if(!c.isClosed()){
                c.close();
            }
        }

        return story;
    }

    public ArrayList<Story> getAllStories(){
        ArrayList<Story> notes = new ArrayList<>();

        Cursor c = db.query(true, StoriesTable.TABLE_NAME, new String[]{
                StoriesTable.COLUMN_URL,
                StoriesTable.COLUMN_TITLE,
                StoriesTable.COLUMN_BYLINE,
                StoriesTable.COLUMN_ABSTRACT,
                StoriesTable.COLUMN_CREATED,
                StoriesTable.COLUMN_THUMB,
                StoriesTable.COLUMN_IMAGE,
                StoriesTable.COLUMN_SECTION}, null, null, null, null, null, null, null);

        if(c != null && c.moveToFirst()){
            do{
                Story note = buildNoteFromCursor(c);
                if(note != null){
                    notes.add(note);
                }
            } while (c.moveToNext());

            if(!c.isClosed()){
                c.close();
            }
        }

        return notes;
    }

    private Story buildNoteFromCursor(Cursor c){
        Story story = null;

        if(c != null){
            story = new Story();
            story.setUrl(c.getString(0));
            story.setTitle(c.getString(1));
            story.setByline(c.getString(2));
            story.setAbstr(c.getString(3));
            story.setCreated(c.getString(4));
            story.setThumbnailUrl(c.getString(5));
            story.setImageUrl(c.getString(6));
            story.setSection(c.getString(7));
        }

        return story;
    }

    public boolean deleteAll(){
        return db.delete(StoriesTable.TABLE_NAME, null, null) > 0;
    }

    public int sectionCount(String section){
        Log.d("d", "checking section count " + section);

        Cursor c = db.query(true,
                StoriesTable.TABLE_NAME,
                new String[]{StoriesTable.COLUMN_TITLE},
                StoriesTable.COLUMN_SECTION + "=?", new String[]{section}, null, null, null, null, null);

//        if(c != null && c.moveToFirst()){
//            note = buildNoteFromCursor(c);
//            if(!c.isClosed()){
//                c.close();
//            }
//        }

        if(c != null && c.moveToFirst()) {
            return c.getCount();
        } else {
            return 0;
        }
    }
}

