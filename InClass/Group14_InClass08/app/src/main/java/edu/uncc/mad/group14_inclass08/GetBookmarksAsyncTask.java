package edu.uncc.mad.group14_inclass08;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 2/22/16.
 */

public class GetBookmarksAsyncTask extends AsyncTask<String, Void, ArrayList<Story>> {
    IStories activity;
    private String key = "d2b659c754de8eb53f08a2e1cac031a7:9:74582574";
    String endpoint = "http://api.nytimes.com/svc/topstories/v1/{section}.json?api-key=" + key;
    Context context;

    public GetBookmarksAsyncTask(IStories activity, Context context){
        this.activity = activity;
        this.context = context;
    }

    @Override
    protected ArrayList<Story> doInBackground(String... params) {
        DataManager dm = new DataManager(this.context);
        ArrayList<Story> stories = dm.getAllStories();
        return stories;
    }

    @Override
    protected void onPostExecute(ArrayList<Story> stories) {
        super.onPostExecute(stories);
        if(stories != null) {
            Log.d("d", "parsed stories: " + stories.toString());
            this.activity.saveStories(stories);
        }
    }

    public interface IStories{
        public void saveStories(ArrayList<Story> stories);
    }
}
