package edu.uncc.mad.group14_inclass08;

/**
 * Created by bradlamotte on 2/22/16.
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import android.util.Log;

/**
 * Created by bradlamotte on 2/17/16.
 */
public class NewsUtil {

    static public class JsonParser{

        static public ArrayList<Story> parse(String input, String section) throws JSONException {
            JSONObject root = new JSONObject(input);
            JSONArray storiesJSON = root.getJSONArray("results");
            ArrayList<Story> stories = new ArrayList<Story>();
            JSONArray multimediaJSON;
            JSONObject multimediaObject;

            for(int i=0; i<storiesJSON.length(); i++){
                JSONObject storyJSON = storiesJSON.getJSONObject(i);
                Story movie = new Story();
                movie.setTitle(storyJSON.getString("title"));
                movie.setByline(storyJSON.getString("byline"));
                movie.setAbstr(storyJSON.getString("abstract"));
                movie.setCreated(storyJSON.getString("created_date"));
                movie.setUrl(storyJSON.getString("url"));
                movie.setSection(section);

                try{
                    multimediaJSON = storyJSON.getJSONArray("multimedia");

                    Log.d("d", "getting multimedia");

                    for(int j=0; j<multimediaJSON.length(); j++){
                        multimediaObject = multimediaJSON.getJSONObject(j);
                        Log.d("d", "multi object " + multimediaObject.toString());

                        String format = multimediaObject.getString("format");
                        Log.d("d", "format " + format);

                        if(format.equals("Standard Thumbnail")){
                            movie.setThumbnailUrl(multimediaObject.getString("url"));
                        } else if(format.equals("Normal")){
                            movie.setImageUrl(multimediaObject.getString("url"));
                        }
                    }
                } catch (JSONException e){

                }

                stories.add(movie);
            }

            return stories;
        }

    }

}
