package edu.uncc.mad.group14_inclass08;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by bradlamotte on 2/22/16.
 */

public class GetStoriesAsynTask extends AsyncTask<String, Void, ArrayList<Story>> {
    IStories activity;
    private String key = "d2b659c754de8eb53f08a2e1cac031a7:9:74582574";
    String endpoint = "http://api.nytimes.com/svc/topstories/v1/{section}.json?api-key=" + key;

    public GetStoriesAsynTask(IStories activity){
        this.activity = activity;
    }

    @Override
    protected ArrayList<Story> doInBackground(String... params) {
        try {
            endpoint = endpoint.replace("{section}", params[0]);
            URL url = new URL(endpoint);
            Log.d("d", "url " + url.toString());
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.connect();
            int statusCode = con.getResponseCode();

            Log.d("d", "statusCode: " + statusCode + ", should be: " + HttpURLConnection.HTTP_OK);

            if(statusCode == HttpURLConnection.HTTP_OK){
                BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuilder builder = new StringBuilder();
                String line = reader.readLine();

                while(line != null){
                    builder.append(line);
                    line = reader.readLine();
                }

                return NewsUtil.JsonParser.parse(builder.toString(), params[0]);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Story> stories) {
        super.onPostExecute(stories);
        if(stories != null) {
            Log.d("d", "parsed stories: " + stories.toString());
            this.activity.saveStories(stories);
        }
    }

    public interface IStories{
        public void saveStories(ArrayList<Story> stories);
    }
}
