package edu.uncc.mad.group14_inclass08;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by bradlamotte on 3/14/16.
 */
public class DataManager {
    private Context mContext;
    private DatabaseOpenHelper dbHelper;
    private SQLiteDatabase db;
    private StoryDAO storyDAO;

    public DataManager(Context mContext) {
        this.mContext = mContext;
        this.dbHelper = new DatabaseOpenHelper(this.mContext);
        this.db = this.dbHelper.getWritableDatabase();
        this.storyDAO = new StoryDAO(db);
    }

    public void close(){
        if(db != null){
            db.close();
        }
    }

    public StoryDAO getNoteDAO(){
        return this.storyDAO;
    }

    public long saveNote(Story note){
        return this.storyDAO.save(note);
    }

    public boolean updateNote(Story note){
        return this.storyDAO.update(note);
    }

    public boolean deleteNOte(Story note){
        return this.storyDAO.delete(note);
    }

    public Story getNote(String url){
        return this.storyDAO.get(url);
    }

    public ArrayList<Story> getAllStories(){
        return this.storyDAO.getAllStories();
    }

    public int sectionCount(String section){
        return this.storyDAO.sectionCount(section);
    }

    public void upgrade(){
        StoriesTable.onUpgrade(this.db, 0, 1);
    }

    public void create(){
        StoriesTable.onCreate(this.db);
    }

    public boolean deleteAll(){
        return this.storyDAO.deleteAll();
    }
}

