package edu.uncc.mad.group14_inclass08;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.lang.reflect.Array;

public class MainActivity extends AppCompatActivity {
    final static String SECTION_KEY = "section";
    DataManager dm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dm = new DataManager(this);

        //dm.create();
        //dm.upgrade();

        String[] sections = {"home", "world",
                "national",
                "politics",
                "nyregion",
                "business",
                "opinion",
                "technology",
                "science",
                "health",
                "sports",
                "arts",
                "fashion",
                "dining",
                "travel",
                "magazine",
                "realestate"};

        for(int i=0; i<sections.length; i++){
            sections[i] = sections[i] + " (" + dm.sectionCount(sections[i]) + ")";
        }

        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.sections, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(this, android.R.layout.simple_list_item_1, sections);
        //String[] sections = getResources().getStringArray(R.array.sections);



        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        findViewById(R.id.submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, StoriesActivity.class);
                String sec = spinner.getSelectedItem().toString();

                int index = sec.lastIndexOf(" (");
                if(index > -1){
                    sec = sec.substring(0, index);
                }

                intent.putExtra(SECTION_KEY, sec);
                startActivity(intent);
            }
        });


    }


}

