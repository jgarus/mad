package edu.uncc.mad.group14_inclass11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

public class AddExpense extends AppCompatActivity {
    Spinner spinner;
    EditText text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        Firebase.setAndroidContext(this);
        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter arrayAdapter = ArrayAdapter.createFromResource(this,R.array.categories,android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);

        findViewById(R.id.button_expense).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Firebase ref = new Firebase(ExpensesList.FIREBASE_URL);

                //ref.child("Expenses/");

                AuthData authData = ref.getAuth();

                Expense expense = new Expense();
                expense.setUser_email(authData.getUid());
                text = (EditText) findViewById(R.id.name_value);
                expense.setName(text.getText().toString());
                text = (EditText) findViewById(R.id.amount_value);
                if(!text.getText().toString().isEmpty())
                    expense.setAmount(Float.parseFloat(text.getText().toString()));
                text = (EditText) findViewById(R.id.date_value);
                expense.setDate(text.getText().toString());
                expense.setCategory(spinner.getSelectedItem().toString());
                Log.d("demo", expense.toString());

                if(expense.isEmpty()){
                    Toast.makeText(AddExpense.this, "Please complete all fields", Toast.LENGTH_SHORT).show();
                } else {

                    Firebase fbNewExpense = ref.child("expenses").push();
                    expense.setKey(fbNewExpense.getKey());
                    fbNewExpense.setValue(expense, new Firebase.CompletionListener() {
                        @Override
                        public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            Intent intent = new Intent(AddExpense.this, ExpensesList.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                }
            }
        });
    }
}
