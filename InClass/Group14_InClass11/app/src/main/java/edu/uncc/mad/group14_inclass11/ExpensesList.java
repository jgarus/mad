package edu.uncc.mad.group14_inclass11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;


public class ExpensesList extends AppCompatActivity {
    static String FIREBASE_URL = "https://sweltering-torch-9125.firebaseio.com/";
    //TextView expense_name = (TextView) findViewById(R.id.expense_name);
    //TextView amount = (TextView) findViewById(R.id.amount);

    Firebase rootRef = new Firebase(FIREBASE_URL);
    Firebase ref = new Firebase(FIREBASE_URL + "expenses");
    static String EXPENSE_KEY = "expense";
    static String EXPENSE_KEY_KEY = "expense_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses_list);
        final ArrayList<Expense> expenses = new ArrayList<>();


        AuthData authData = rootRef.getAuth();
        ref.orderByChild("user_email").equalTo(authData.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                expenses.clear();

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Expense expense = postSnapshot.getValue(Expense.class);
                    expenses.add(expense);
                    Log.d("d", "expenses: " + expenses.toString());
                }

                ListView listView = (ListView) findViewById(R.id.expense_listview);
                ExpensesAdapter adapter = new ExpensesAdapter(ExpensesList.this, R.layout.expenses_listview, expenses);
                adapter.setNotifyOnChange(true);
                listView.setAdapter(adapter);

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Expense expense = expenses.get(position);
                        Log.d("d", "sending expense " + expense.toString());
                        Intent intent = new Intent(ExpensesList.this, ExpenseDetails.class);
                        intent.putExtra(EXPENSE_KEY, expense);
                        //intent.putExtra(EXPENSE_KEY_KEY, expense.)
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(ExpensesList.this, "Canceled", Toast.LENGTH_SHORT).show();
            }
        });

    }



    //Create Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;

        switch (item.getItemId()) {
            //Start add expense activity
            case R.id.menu_add_expense:
                intent = new Intent(this, AddExpense.class);
                startActivity(intent);
                return true;


            //logout and return to login activity
            case R.id.menu_logout:
                Firebase ref = new Firebase(FIREBASE_URL);
                ref.unauth();
                intent = new Intent(ExpensesList.this, LoginActivity.class);
                startActivity(intent);
                this.finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
