package edu.uncc.mad.group14_inclass11;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;

public class ExpenseDetails extends AppCompatActivity {
    TextView textView;
    String expense_key = "";
    Expense expense;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expense_details);
        Firebase.setAndroidContext(this);

        expense = (Expense) getIntent().getExtras().getSerializable(ExpensesList.EXPENSE_KEY);
        //expense_key = getIntent().getExtras().getString(ExpensesList.EXPENSE_KEY_KEY);
        Log.d("d", "expense key " + expense_key);

        textView = (TextView) findViewById(R.id.details_name_value);
        textView.setText(expense.getName());

        textView = (TextView) findViewById(R.id.details_category_value);
        textView.setText(expense.getCategory());
        textView = (TextView) findViewById(R.id.details_amount_value);
        textView.setText(String.valueOf(expense.getAmount()));
        textView = (TextView) findViewById(R.id.details_date_value);
        textView.setText(expense.getDate().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.details_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_expense:
                Firebase ref = new Firebase(ExpensesList.FIREBASE_URL);
                ref.child("expenses").child(expense.getKey()).setValue(null, new Firebase.CompletionListener() {

                    @Override
                    public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                        Toast.makeText(ExpenseDetails.this, "Expense successfully deleted", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                });

            default:
                return super.onOptionsItemSelected(item);
        }


    }
}
