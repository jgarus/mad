package edu.uncc.mad.group14_inclass07;

import android.util.Log;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jesus on 2/22/16.
 */
public class Story implements Serializable {

    private String title, byline, abstr, thumbnailUrl, imageUrl;
    Date created;

    public Story(){}

    public Story(String title, String byline, String astr, Date created, String thumbnailUrl, String imageUrl){
        this.title = title;
        this.byline = byline;
        this.abstr = abstr;
        this.created = created;
        this.thumbnailUrl = thumbnailUrl;
        this.imageUrl = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getByline() {
        return byline;
    }

    public void setByline(String byline) {
        this.byline = byline;
    }

    public String getAbstr() {
        return abstr;
    }

    public void setAbstr(String abstr) {
        this.abstr = abstr;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreated(){
        //MM/DD/YYYY HH:MM AM/ PM
        //SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd");
        return dateFormat.format(this.created);
    }

    public void setCreated(String d){
        //2016-02-27T23:44:08-5:00
        //SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        try {
            this.created = dateFormat.parse(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Story{" +
                "title='" + title + '\'' +
                ", byline='" + byline + '\'' +
                ", abstr='" + abstr + '\'' +
                ", thumbnailUrl='" + thumbnailUrl + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", created=" + created +
                '}';
    }
}
