package edu.uncc.mad.group14_inclass07;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class StoriesActivity extends AppCompatActivity implements GetStoriesAsynTask.IStories {
    final static String STORY_KEY = "story";
    ProgressDialog progress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories);

        String section = getIntent().getExtras().getString(MainActivity.SECTION_KEY);
        //Log.d("d", "stories url " + storiesUrl);

        showGetStoriesProgress();
        new GetStoriesAsynTask(StoriesActivity.this).execute(section);
    }

    @Override
    public void saveStories(ArrayList<Story> stories) {
        //Log.d("d", "saved stories " + stories.toString());
        final ArrayList<Story> storiesNew;
        storiesNew = stories;

        ListView listView = (ListView) findViewById(R.id.listView);
        StoryAdapter adapter = new StoryAdapter(this, R.layout.story_view, stories);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("d", "position: " + position);
                Story story = (Story) storiesNew.get(position);
                Intent intent = new Intent(StoriesActivity.this, NewsDetailsActivity.class);
                intent.putExtra(STORY_KEY, story);
                startActivity(intent);
            }
        });

        progress.dismiss();
    }

    public void showGetStoriesProgress(){
        progress = new ProgressDialog(StoriesActivity.this);
        progress.setMessage("Loading Stories");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(false);
        progress.show();
    }

}
